﻿namespace QuickRoster.App.Enums
{
    public enum RoleEnum
    {
        Admin,
        User,
        None = -1
    }
}
