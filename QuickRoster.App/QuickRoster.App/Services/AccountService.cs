﻿namespace QuickRoster.App.Services
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Enums;
    using Models.DTO;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using QuickRoster.App.Models;
    using Resources;
    using Xamarin.Forms;
    using User = Models.User;

    public class AccountService
    {
        private readonly string BaseURL = AppStringResources.AzureApiBaseUrl + AppStringResources.AccountAPI;

        public async Task<User> Authenticate(GoogleUser user)
        {
            var requestUrl = string.Format(AppStringResources.LoginOrRegisterAccountRequest, this.BaseURL, user.Email, user.Name);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);

            return new User
            {
                Role = await ParseRole(response),
                ServerAuthenticationCode = "AuthCode",
                Email = user.Email,
                MemberOfBusiness = response.StatusCode == HttpStatusCode.OK
            };
        }

        public async Task<HttpStatusCode> CreateAccount(User user, string email)
        {
            var requestUrl = string.Format(AppStringResources.CreateAccountRequest, this.BaseURL, email, user.Email);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);

            return response.StatusCode;
        }

        public async Task<HttpStatusCode> JoinBusiness(User user, string code)
        {
            var requestUrl = string.Format(AppStringResources.JoinBusinessRequest, this.BaseURL, user.Email, code);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);

            user.Role = await ParseRole(response);

            return response.StatusCode;
        }

        public async Task<HttpStatusCode> EditAvailibility(User user, ICollection<Availibility> availibility)
        {
            var requestUrl = string.Format(AppStringResources.EditAvailibilityRequest, this.BaseURL, user.Email);

            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(availibility), Encoding.UTF8, "application/json");
            var result = await client.PostAsync(requestUrl, content);

            return result.StatusCode;

        }

        private async Task<RoleEnum> ParseRole(HttpResponseMessage response)
        {
            int role;
            string responseMessage = await response.Content.ReadAsStringAsync();
            if (!int.TryParse(responseMessage, out role))
            {
                role = -1;
            }

            return (RoleEnum)role;
        }

        public async Task<IEnumerable<RosterDTO>> GetRosters(string email)
        {
            var requestUrl = string.Format(AppStringResources.GetRostersRequest, this.BaseURL, email);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseMessage = await response.Content.ReadAsStringAsync();
                IEnumerable<RosterDTO> rosterTemplate = JsonConvert.DeserializeObject<IEnumerable<RosterDTO>>(responseMessage);
                return rosterTemplate;
            }
            return null;
        }
    }
}
