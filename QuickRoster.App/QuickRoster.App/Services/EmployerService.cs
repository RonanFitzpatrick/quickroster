﻿namespace QuickRoster.App.Services
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Models;
    using Models.DTO;
    using Newtonsoft.Json;
    using Resources;
    using ViewModels;
    using Shift = Models.DTO.Shift;

    public class EmployerService
    {
        private readonly string BaseURL = AppStringResources.AzureApiBaseUrl + AppStringResources.EmployerAPI;
        private UserViewModel UserViewModel;

        public EmployerService(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
        }

        public async Task<HttpStatusCode> RegisterBusiness(string employerName, string employerEmail)
        {
            //string employerEmail, string userEmail, string employerName
            var requestUrl = string.Format(AppStringResources.RegisterEmployerRequest, this.BaseURL,employerEmail,this.UserViewModel.User.Email,employerName);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);

            return response.StatusCode;
        }

        public async Task<HttpStatusCode> CreateTemplate(RosterTemplate template)
        {
            var requestUrl = string.Format(AppStringResources.TemplateRequest, this.BaseURL, this.UserViewModel.User.Email);
            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(template), Encoding.UTF8, "application/json");
            var result = await client.PostAsync(requestUrl, content);

            return result.StatusCode;
        }


        public async Task<IEnumerable<string>> GetTemplateNames()
        {
            var requestUrl = string.Format(AppStringResources.GetTemplateNamesRequest, this.BaseURL, this.UserViewModel.User.Email);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseMessage = await response.Content.ReadAsStringAsync();
                IEnumerable<string> templateNames = JsonConvert.DeserializeObject<IEnumerable<string>>(responseMessage);
                return templateNames;
            }
            return null;
        }


        public async Task<IEnumerable<Models.DTO.User>> GetEmployees()
        {
            var requestUrl = string.Format(AppStringResources.GetEmployeesRequest, this.BaseURL, this.UserViewModel.User.Email);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseMessage = await response.Content.ReadAsStringAsync();
                IEnumerable<Models.DTO.User> templateNames = JsonConvert.DeserializeObject<IEnumerable<Models.DTO.User>>(responseMessage);
                return templateNames;
            }
            return null;
        }

        public async Task<RosterTemplate> GetRosterTemplate(string templateName)
        {
            var requestUrl = string.Format(AppStringResources.getRosterTemplateRequest, this.BaseURL, this.UserViewModel.User.Email, templateName);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(requestUrl);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseMessage = await response.Content.ReadAsStringAsync();
                RosterTemplate rosterTemplate = JsonConvert.DeserializeObject<RosterTemplate>(responseMessage);
                return rosterTemplate;
            }
            return null;
        }

        public async Task SaveRoster(List<Shift> assignedShifts)
        {
            var requestUrl = string.Format(AppStringResources.SaveRosterRequest, this.BaseURL, this.UserViewModel.User.Email);

            var httpClient = new HttpClient();

            var content = new StringContent(JsonConvert.SerializeObject(assignedShifts), Encoding.UTF8, "application/json");
            await httpClient.PostAsync(requestUrl, content);
        }
    
    }
}
