﻿using System.Net.Http;
using System.Threading.Tasks;
using QuickRoster.App.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QuickRoster.App.Services
{
    using Resources;

    public class GoogleServices
    {
        public static readonly string ClientId = Keys.ClientID;
        public static readonly string ClientSecret = Keys.GoogleSecret;
        //replace with custom page
        public static readonly string RedirectUri = AppStringResources.RedirectUrl;

        public async Task<string> GetAccessTokenAsync(string code)
        {
            var requestUrl = string.Format(AppStringResources.GoogleAccessTokenRequest,code, ClientId, ClientSecret);
            var httpClient = new HttpClient();

            var response = await httpClient.PostAsync(requestUrl, null);

            var json = await response.Content.ReadAsStringAsync();

            var accessToken = JsonConvert.DeserializeObject<JObject>(json).Value<string>(AppStringResources.Access_Token);

            return json;
        }

        public async Task<GoogleUser> GetGoogleUserProfileAsync(string accessToken)
        {

            var requestUrl = string.Format(AppStringResources.GoogleUserRequest, accessToken);

            var httpClient = new HttpClient();

            var userJson = await httpClient.GetStringAsync(requestUrl);

            var googleProfile = JsonConvert.DeserializeObject<GoogleProfile>(userJson);
            Profile profile = JsonConvert.DeserializeObject<Profile>(await GetGmailUserProfileAsync(googleProfile.Id, accessToken));

            return new GoogleUser(googleProfile.Name.GetFullName(),profile.emailAddress);
        }

        public async Task<string> GetGmailUserProfileAsync(string userId, string accessToken)
        {

            var requestUrl = string.Format(AppStringResources.GmailUserRequest,userId,accessToken);
            var httpClient = new HttpClient();

            var userJson = await httpClient.GetStringAsync(requestUrl);

            return userJson;
        }
    }
}
