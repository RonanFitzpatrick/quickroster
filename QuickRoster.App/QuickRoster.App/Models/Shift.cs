﻿namespace QuickRoster.App.Models
{
    using Xamarin.Forms;

    public class Shift
    {
        public Picker Day { get; set; }

        public Picker StartTime { get; set; }

        public Picker EndTime { get; set; }

        public Picker Skill { get; set; }
    }
}
