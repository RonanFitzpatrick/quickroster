﻿namespace QuickRoster.App.Models
{
    public class Profile
    {
        public string emailAddress { get; set; }
        public int messagesTotal { get; set; }
        public int threadsTotal { get; set; }
        public long historyId { get; set; }
    }
}
