﻿namespace QuickRoster.App.Models
{
    using Enums;

    public class User
    {
        public  RoleEnum Role { get; set; }

        public string ServerAuthenticationCode { get; set; }

        public string Email { get; set; }

        public bool MemberOfBusiness { get; set; }
    }
}
