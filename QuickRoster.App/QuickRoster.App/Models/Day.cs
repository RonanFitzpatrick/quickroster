﻿namespace QuickRoster.App.Models
{
    public class Day
    {
        public string ShortHand { get; set; }
        public string LongHand { get; set; }
    }
}
