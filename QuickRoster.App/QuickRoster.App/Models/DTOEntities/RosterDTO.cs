﻿namespace QuickRoster.App.Models.DTO
{
    using System;
    using System.Collections.Generic;

    public class RosterDTO
    {
        public IEnumerable<ShiftDTO> Shifts{get;set;}

        public DateTime WeekStarting { get; set; }
    }
}
