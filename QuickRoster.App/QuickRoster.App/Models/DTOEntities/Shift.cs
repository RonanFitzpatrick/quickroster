﻿namespace QuickRoster.App.Models.DTO
{


    public class Shift
    {
        public int TemplateId { get; set; }

        public int ShiftTemplateId { get; set; }

        public int UserId { get; set; }
    }
}
