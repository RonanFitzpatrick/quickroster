﻿namespace QuickRoster.App.Models.DTO
{
    public class ShiftTemplate
    {

        public string StartHour { get; set; }


        public string FinishHour{ get; set; }

        public Enums.Day Day{ get; set; }


        public Skill SkillRequired { get; set; }

        public int Id { get; set; }
    }
}
