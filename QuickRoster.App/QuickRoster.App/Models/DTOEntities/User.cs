﻿namespace QuickRoster.App.Models.DTO
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public class User
    {
        public int Id { get; set; }

        public int? Role { get; set; }

        public int? EmployerId { get; set; }

        public ICollection<Skill> Skills { get; set; }

        public string Email { get; set; }

        public ICollection<Shift> Shifts { get; set;}

        public string Name { get; set; }
    }
}
