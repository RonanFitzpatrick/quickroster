﻿namespace QuickRoster.App.Models.DTO
{
    using System.Collections.Generic;

    public class RosterTemplate
    {
        public ICollection<ShiftTemplate> TemplateShifts { get; set; }

        public string TemplateName { get; set; }

        public int Id { get; set; }

        public int EmployerId { get; set; }
    }
}
