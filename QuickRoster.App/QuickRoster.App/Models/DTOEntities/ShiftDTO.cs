﻿namespace QuickRoster.App.Models.DTO
{
    public class ShiftDTO
    {
        public string Name { get; set; }

        public string Skill { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public int Day { get; set; }
    }
}
