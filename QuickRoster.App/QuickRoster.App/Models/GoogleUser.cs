﻿namespace QuickRoster.App.Models
{
    public class GoogleUser
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public GoogleUser(string name, string email)
        {
            this.Name = name;
            this.Email = email;
        }
    }
}
