﻿namespace QuickRoster.App.Views
{
    using Models.DTO;
    using Resources;
    using ViewModels;
    using System.Collections.Generic;
    using System.Linq;
    using Xamarin.Forms;

    public class ViewRostersPage : ContentPage
    {
        private UserViewModel UserViewModel;
        IEnumerable<RosterDTO> Rosters;
        public ViewRostersPage(UserViewModel userViewModel, IEnumerable<RosterDTO> rosters)
        {
            this.Rosters = rosters;

            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.Rosters;
            this.BackgroundColor = Color.White;

            ListView view = new ListView();

            view.ItemsSource = rosters.Select(x => x.WeekStarting.Day +"/" + x.WeekStarting.Month + "/" + x.WeekStarting.Year);

            view.ItemSelected += View_ItemSelected;
            this.Content = view;
        }


        private void View_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            RosterDTO roster = this.Rosters.FirstOrDefault(
                x =>
                    x.WeekStarting.Day + "/" + x.WeekStarting.Month + "/" + x.WeekStarting.Year ==
                    e.SelectedItem.ToString());
            ViewSingleRoster(roster);
        }

        private async void ViewSingleRoster(RosterDTO roster)
        {
            await Navigation.PushAsync(new RosterPage(roster));
        }
    }
}
