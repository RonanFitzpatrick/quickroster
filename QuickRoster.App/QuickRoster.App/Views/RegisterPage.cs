﻿namespace QuickRoster.App.Views
{
    using System;
    using System.Net;
    using Components;
    using Resources;
    using ViewModels;
    using Xamarin.Forms;

    public class RegisterPage : ContentPage
    {
        private CustomGreenButton SubmitButton, ResgisterEmployer;
        private UserViewModel UserViewModel;
        private Entry CodeField;
        private ErrorLabel CodeError;
        public RegisterPage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            SetContent();

            this.Title = AppStringResources.Register;
            this.BackgroundColor = Color.White;
        }

        public void SetContent()
        {
            this.SetButtons();
            CodeField = new Entry { Placeholder = AppStringResources.FourDigitCode };
            this.CodeError = new ErrorLabel();
            this.Content = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Children =
                {
                    this.GetLogo(),
                    new CustomLabelForm(AppStringResources.RegisterEmployee + " " + AppStringResources.FourDigitCode),
                    CodeField,
                    CodeError,
                    this.SubmitButton,
                    this.ResgisterEmployer
                }
            };
        }

        private void SetButtons()
        {
            this.SubmitButton = new CustomGreenButton(AppStringResources.Submit);
            this.ResgisterEmployer = new CustomGreenButton(AppStringResources.Register);

            this.SubmitButton.Clicked += this.SubmitRegister;
            this.ResgisterEmployer.Clicked += this.RegisterEmployer;
        }

        private async void RegisterEmployer(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RegisterEmployerPage(this.UserViewModel));
        }

        private void EnableButtons(bool enable)
        {
            this.SubmitButton.IsEnabled = enable;
            this.ResgisterEmployer.IsEnabled = enable;
        }

        private async void SubmitRegister(object sender, EventArgs e)
        {
            var code = this.CodeField.Text;

            EnableButtons(false);

            if (string.IsNullOrWhiteSpace(code) || code.Length != 8)
            {
                this.CodeError.ErrorMessage(AppStringResources.CodeError);
                EnableButtons(true);
                return;
            }

            HttpStatusCode status = await this.UserViewModel.JoinBusiness(this.CodeField.Text);

            if (status == HttpStatusCode.OK)
            {
                EnableButtons(true);
                await Navigation.PushAsync(this.UserViewModel.GetHomePage());
                return;
            }

            this.CodeError.ErrorMessage(AppStringResources.SomethingWentWrong);
            EnableButtons(true);
        }

        private Image GetLogo()
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      AppStringResources.LogoFileName,
                      AppStringResources.LogoFileName,
                      AppStringResources.LogoFileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource
            };

        }
    }
}
