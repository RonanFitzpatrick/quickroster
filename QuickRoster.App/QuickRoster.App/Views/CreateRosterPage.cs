﻿namespace QuickRoster.App.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Components;
    using Models.DTO;
    using Resources;
    using ViewModels;
    using Xamarin.Forms;
    using User = Models.DTO.User;

    public class CreateRosterPage : ContentPage
    {
        private UserViewModel UserViewModel;
        private IEnumerable<string> RosterTemplateList;
        private IEnumerable<User> EmployeeList;
        private StackLayout Layout = new StackLayout();
        private Grid Employees;
        private Grid Shifts;
        private Picker TemplatePicker;
        private Dictionary<string, Grid> EmployeeGrid = new Dictionary<string, Grid>();
        private List<Models.DTO.Shift> AssignedShifts = new List<Models.DTO.Shift>();
        private RosterTemplate Template;

        public CreateRosterPage(UserViewModel userViewModel, IEnumerable<string> rosterTemplateList, IEnumerable<Models.DTO.User> employeeList)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.CreateRoster;
            this.BackgroundColor = Color.White;

            this.EmployeeList = employeeList;
            this.RosterTemplateList = rosterTemplateList;

            var scroll = new ScrollView();
            Content = scroll;

            var menu = this.GetMenuGrid();
            View shiftAssignment = GetShiftAssignmentGrid();

            CustomLabelForm chooseATemplateLabel = new CustomLabelForm(AppStringResources.ChooseATemplate);
            chooseATemplateLabel.HorizontalOptions = LayoutOptions.Center;
            Grid grid = new Grid();
            grid.Children.Add(chooseATemplateLabel,3,0);

            grid.Children.Add(this.SaveChangesImage(), 6, 0);
            this.Layout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                Children =
                {
                    grid,
                    menu,
                    shiftAssignment
                }
            };

            scroll.Content = this.Layout;
        }

        private View GetEmployeeGrid()
        {
            Frame frame = new Frame();
            frame.OutlineColor = Color.Black;
            frame.BackgroundColor = Color.White;
            StackLayout layout = new StackLayout();
            Grid grid = new Grid();
            grid.HeightRequest = this.MinimumHeightRequest;

            CustomLabelForm EmployeeList = new CustomLabelForm(AppStringResources.EmployeeList);
            EmployeeList.HorizontalOptions = LayoutOptions.Center;

            grid.Children.Add(EmployeeList,0,0);
            this.AddEmployees(grid);

            layout.Children.Add(EmployeeList);
            layout.Children.Add(grid);

            frame.Content = layout;

            return frame;
        }

        private void AddEmployees(Grid grid)
        {
            foreach (var employee in this.EmployeeList)
            {
                Grid employeeGrid = new Grid();
                this.EmployeeGrid.Add(employee.Name,employeeGrid);

                Frame frame = new Frame();
                frame.OutlineColor = Color.Black;
                frame.BackgroundColor = Color.White;

                CustomLabelForm nameTag = new CustomLabelForm(AppStringResources.Name);
                CustomLabelForm name = new CustomLabelForm(employee.Name);
                CustomLabelForm contractedHoursTag = new CustomLabelForm(AppStringResources.ContractedHours);
                CustomLabelForm contractedValue = new CustomLabelForm("30");
                CustomLabelForm skillTag = new CustomLabelForm(AppStringResources.Skills);
                CustomLabelForm skills = this.CreateSkillLabel(employee);
                StackLayout shifts = new StackLayout();
                shifts.Children.Add(new CustomLabelForm(AppStringResources.Shifts));

                employeeGrid.Children.Add(nameTag,0,0);
                employeeGrid.Children.Add(name, 0, 1);
                employeeGrid.Children.Add(contractedHoursTag, 1, 0);
                employeeGrid.Children.Add(contractedValue, 1, 1);
                employeeGrid.Children.Add(skillTag, 0, 2);
                employeeGrid.Children.Add(skills, 0, 3);
                employeeGrid.Children.Add(shifts,0,4);
                frame.Content = employeeGrid;

                grid.Children.Add(frame,0,grid.Children.Count);
            }
        }

        private CustomLabelForm CreateSkillLabel(User user)
        {
           user.Skills = new List<Skill>
           {
               new Skill
               {
                   SkillName = "Cashier"
               },
                new Skill
               {
                   SkillName = "Supervisor"
               },
                 new Skill
               {
                   SkillName = "Deli"
               }
           };

            string labelContent = "";

            foreach (var skill in user.Skills)
            {
                labelContent += skill.SkillName + ", ";
            }

            return new CustomLabelForm(labelContent);
        }

        private View GetShiftAssignmentGrid()
        {
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.Children.Add(this.GetShiftGrid(),0,0);
            grid.Children.Add(this.GetEmployeeGrid(),1,0);
            return grid;
        }

        private View GetShiftGrid()
        {

            Frame frame = new Frame();
            StackLayout layout = new StackLayout();
            frame.BackgroundColor = Color.White;
            frame.OutlineColor = Color.Black;
            this.Shifts = new Grid();
            this.Shifts.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            this.Shifts.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            CustomLabelForm shiftListLabel = new CustomLabelForm(AppStringResources.ShiftList);
            shiftListLabel.HorizontalOptions = LayoutOptions.Center;

            this.Shifts.Children.Add(shiftListLabel, 0, 0);

            layout.Children.Add(shiftListLabel);
            layout.Children.Add(this.Shifts);

            frame.Content = layout;

            return frame;
        }

        private Grid GetMenuGrid()
        {
            Grid menuGrid = new Grid();

            this.TemplatePicker = new Picker();
            this.TemplatePicker.SelectedIndexChanged += TemplatePickerOnSelectedIndexChanged;

            foreach (var templateName in this.RosterTemplateList)
            {
                this.TemplatePicker.Items.Add(templateName);
            }

            menuGrid.Children.Add(this.TemplatePicker, 0,0);
            return menuGrid;
        }

        private async void TemplatePickerOnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            string templateName = this.TemplatePicker.Items[this.TemplatePicker.SelectedIndex];
            this.Template = await this.UserViewModel.GetRosterTemplate(templateName);

            int count = 1;

            foreach (var shift in this.Template.TemplateShifts)
            {
                Grid shiftGrid = CreateShiftGrid(shift);

                Frame frame = new Frame();
                frame.BackgroundColor = Color.White;
                frame.OutlineColor = Color.Black;
                frame.Content = shiftGrid;

                this.Shifts.Children.Add(frame,0,count);
                count++;
            }
        }

        private Grid CreateShiftGrid(ShiftTemplate shift)
        {
            CustomShiftGrid shiftGrid = new CustomShiftGrid(shift);

            shiftGrid.Assign.Clicked += AssignShift;

            foreach (var employee in this.EmployeeList)
            {
                shiftGrid.EmployeePicker.Items.Add(employee.Name);
            }

            return shiftGrid;
        }

        private void AssignShift(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            CustomShiftGrid grid = (CustomShiftGrid) button.Parent;
            Picker picker = (Picker) grid.Children.FirstOrDefault(x => x.GetType() == typeof(Picker));

            var startTime = ((Label) GetView(grid, 1, 1)).Text;
            var finishTime = ((Label) GetView(grid, 2, 1)).Text;
            var day = ((Label) GetView(grid, 0, 1)).Text;

            CustomLabelForm shift = new CustomLabelForm(day + ": " + startTime + " - " + finishTime);

            var employeeName = picker.Items[picker.SelectedIndex];
            StackLayout shifts =
                (StackLayout)
                this.EmployeeGrid[employeeName].Children.FirstOrDefault(x => x.GetType() == typeof(StackLayout));

            shifts.Children.Add(shift);
            User user = EmployeeList.FirstOrDefault(x => x.Name.Equals(employeeName));
            AssignedShifts.Add(new Shift
            {
                TemplateId = this.Template.Id,
                ShiftTemplateId = grid.Template.Id,
                UserId = user.Id
            });
            RemoveRow(grid);
        }

        private void RemoveRow(Grid grid)
        {
            var row = Grid.GetRow(grid);
            Grid mainGrid = (Grid)grid.Parent.Parent;
            var children = mainGrid.Children.ToList();

            foreach (var child in children.Where(child => Grid.GetRow(child) == row +1))
            {
                mainGrid.Children.Remove(child);
            }

            foreach (var child in children.Where(child => Grid.GetRow(child) > row))
            {
                Grid.SetRow(child, Grid.GetRow(child) - 1);
            }
        }

        public View GetView(Grid grid,int col, int row)
        {
            foreach (View v in grid.Children)
            {
                if ((col == Grid.GetColumn(v)) && (row == Grid.GetRow(v)))
                {
                    return v;
                }
            }
            return null;
        }


        private Image SaveChangesImage()
        {
            Image saveTemplateImg = this.GetImage(AppStringResources.SaveFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.SaveRoster),
                NumberOfTapsRequired = 1
            };

            saveTemplateImg.GestureRecognizers.Add(tapGestureRecognizer);

            return saveTemplateImg;
        }

        private async void SaveRoster()
        {
            await this.UserViewModel.SaveRoster(AssignedShifts);
        }


        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource,
                Margin = new Thickness(0, 0, 10, 0)
            };

        }
    }
}
