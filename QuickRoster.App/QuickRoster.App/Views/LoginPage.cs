﻿namespace QuickRoster.App.Views
{
    using System.Linq;
    using QuickRoster.App.Services;
    using QuickRoster.App.ViewModels;
    using Resources;
    using Xamarin.Forms;

    public class LoginPage : ContentPage
    {

        private readonly GoogleUserViewModel GoogleUserViewModel = new GoogleUserViewModel();
        private readonly UserViewModel UserViewModel = new UserViewModel();

        public LoginPage()
        {
            BindingContext = this.GoogleUserViewModel;

            Title = AppStringResources.Loading;
            BackgroundColor = Color.White;

            var authRequest = string.Format(AppStringResources.GoogleAuthRequest, GoogleServices.RedirectUri, GoogleServices.ClientId);

            var webView = new WebView
            {
                Source = authRequest,
                HeightRequest = 1
            };
            webView.Navigated += WebViewOnNavigated;

            Content = webView;
        }

        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            var code = ExtractCodeFromUrl(e.Url);

            if (code != "")
            {
                await this.GoogleUserViewModel.SetGoogleUserProfileAsync(code);
                await this.UserViewModel.SetUser(this.GoogleUserViewModel.User);
                SetPageContent();
            }
        }

        private async void SetPageContent()
        {
            await Navigation.PushAsync(this.UserViewModel.GetHomePage());
        }

        private string ExtractCodeFromUrl(string url)
        {
            if (url.Contains("access_token="))
            {
                var attributes = url.Split('&');

                var code = attributes.FirstOrDefault(s => s.Contains("access_token=")).Split('=')[1];

                return code;
            }

            return string.Empty;
        }
    }
}
