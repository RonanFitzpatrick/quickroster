﻿using System;
using Xamarin.Forms;

namespace QuickRoster.App.Views
{
    using Components;
    using Resources;

    public class MainCsPage : ContentPage
    {

        public MainCsPage()
        {
            Title = AppStringResources.QuickRoster;
            BackgroundColor = Color.White;
            this.SetContent();
        }

        private void SetContent()
        {
            var loginButton = new CustomGreenButton(AppStringResources.Login);
            loginButton.Clicked += LoginWithFacebook_Clicked;

            var stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Children =
                {
                   this.getLogo(),
                   loginButton
                }
            };

            this.Content = new ScrollView()
            {
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Content = stack
            };
        }

        private async void LoginWithFacebook_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage());
        }

        private Image getLogo()
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      AppStringResources.LogoFileName,
                      AppStringResources.LogoFileName,
                      AppStringResources.LogoFileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource
            };

        }
    }
}

