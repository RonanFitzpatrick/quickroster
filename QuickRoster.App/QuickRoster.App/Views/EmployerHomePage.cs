﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace QuickRoster.App.Views
{
    using Components;
    using Resources;
    using ViewModels;

    public class EmployerHomePage : ContentPage
    {
        private UserViewModel UserViewModel;

        public EmployerHomePage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.Home;
            this.BackgroundColor = Color.White;
            SetContent();
        }

        private void SetContent()
        {
            var grid = new Grid
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                RowSpacing = 10,
                ColumnSpacing = 200
            };

            grid.Children.Add(this.CreateRosterImage(), 0, 0);
            grid.Children.Add(PlaceHolder(), 0, 1);
            grid.Children.Add(PlaceHolder(), 0, 2);
            grid.Children.Add(this.CreateNewAddEmployee(), 1, 0);
            grid.Children.Add(PlaceHolder(), 1, 1);
            grid.Children.Add(PlaceHolder(), 1, 2);
            grid.Children.Add(this.CreateNewTemplate(), 2, 0);
            grid.Children.Add(PlaceHolder(), 2, 1);
            grid.Children.Add(PlaceHolder(), 2, 2);
            this.Content = grid;
        }

        private View CreateNewTemplate()
        {
            Image newRoster = this.GetImage(AppStringResources.TemplateFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.CreateTemplate),
                NumberOfTapsRequired = 1
            };

            newRoster.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack(AppStringResources.NewTemplate, newRoster);
        }

        private StackLayout PlaceHolder()
        {
            Image newRoster = this.GetImage(AppStringResources.PlaceHolder);
            newRoster.Margin = 10;
            return CreateStack("Place Holder", newRoster);
        }

        private StackLayout CreateRosterImage()
        {
            Image newRoster = this.GetImage(AppStringResources.CreateRosterFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.CreateNewRoster),
                NumberOfTapsRequired = 1
            };

            newRoster.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack(AppStringResources.CreateRoster, newRoster);
        }

        private StackLayout CreateNewAddEmployee()
        {
            Image addEmployeeImage = this.GetImage(AppStringResources.AddFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.AddEmployees),
                NumberOfTapsRequired = 1
            };

            addEmployeeImage.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack(AppStringResources.AddEmployees, addEmployeeImage);
        }

        private StackLayout CreateStack(string labelText, Image image)
        {
            Label label = new CustomLabelForm(labelText);
            label.HorizontalTextAlignment = TextAlignment.Center;

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = 10,
                Children =
            {
                image,
                label
            }
            };
        }

        private async void CreateNewRoster()
        {
            IEnumerable<string> templateNames =  await this.UserViewModel.GetRosterTemplateList();
            IEnumerable<Models.DTO.User> Employees = await this.UserViewModel.GetEmployeeList();
            await Navigation.PushAsync(new CreateRosterPage(this.UserViewModel, templateNames, Employees));
        }

        private async void CreateTemplate()
        {
            await Navigation.PushAsync(new CreateTemplatePage(this.UserViewModel));
        }

        private async void AddEmployees()
        {
            await Navigation.PushAsync(new AddEmployeesPage(this.UserViewModel));
        }


        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource
            };

        }
    }
}
