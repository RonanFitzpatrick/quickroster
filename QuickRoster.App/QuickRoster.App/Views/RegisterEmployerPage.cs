﻿namespace QuickRoster.App.Views
{
    using System;
    using System.Net;
    using Behaviors;
    using Components;
    using Resources;
    using Services;
    using ViewModels;
    using Xamarin.Forms;

    public class RegisterEmployerPage : ContentPage
    {
        private Entry Email;
        private Entry Name;
        private Label EmailErrorLabel;
        private Label NameErrorLabel;
        private Label SubmitErrorLabel;
        private CustomGreenButton RegisterButton;
        private UserViewModel UserViewModel;
        private EmployerService EmployerService;

        public RegisterEmployerPage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.Register;
            this.BackgroundColor = Color.White;
            this.EmployerService = new EmployerService(userViewModel);

            SetContent();
        }

        private void SetContent()
        {
            this.SetupComponents();

            this.Content = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Margin = 10,
                Children =
                {
                    new CustomLabelForm(AppStringResources.EmployerName),
                    this.Name,
                    this.NameErrorLabel,
                    new CustomLabelForm(AppStringResources.BusinessEmail),
                    this.Email,
                    this.EmailErrorLabel,
                    this.RegisterButton,
                    this.SubmitErrorLabel
                }
            };
        }

        private void SetupComponents()
        {
            this.EmailErrorLabel = new ErrorLabel();
            this.NameErrorLabel = new ErrorLabel();
            this.SubmitErrorLabel = new ErrorLabel();

            this.Email = new Entry
            {
                Placeholder = AppStringResources.Email,
                Behaviors = { new EmailValidatorBehavior(this.EmailErrorLabel) },
            };

            this.Name = new Entry
            {
                Placeholder = AppStringResources.Name
            };

            this.RegisterButton = new CustomGreenButton(AppStringResources.Register);
            this.RegisterButton.Clicked += this.Register;
        }


        private async void Register(object sender, EventArgs e)
        {
            if (FormValid())
            {
                this.SubmitErrorLabel.Text = string.Empty;
                var response = await this.EmployerService.RegisterBusiness(this.Name.Text, this.Email.Text);
                if (response == HttpStatusCode.Created)
                {
                    await Navigation.PushAsync(this.UserViewModel.GetHomePage());
                }
                else if(response == HttpStatusCode.Conflict)
                {
                    this.SubmitErrorLabel.Text = AppStringResources.AlreadyRegistered;
                }
                return;
            }

            this.SubmitErrorLabel.Text = AppStringResources.Invalid;
        }

        private bool FormValid()
        {
            return !string.IsNullOrEmpty(this.Name.Text);
        }
    }
}
