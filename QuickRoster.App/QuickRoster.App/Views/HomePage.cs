﻿namespace QuickRoster.App.Views
{
    using Components;
    using Xamarin.Forms;
    using Resources;
    using ViewModels;

    public class HomePage : ContentPage
    {

        private UserViewModel UserViewModel;

        public HomePage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;

            this.Title = AppStringResources.Home;
            this.BackgroundColor = Color.White;

            if (Device.Idiom == TargetIdiom.Phone)
            {
                this.Content = this.PhoneGrid();
            }
            else
            {
                this.Content = this.BigGrid();
            }
        }

        private View BigGrid()
        {
            var grid = new Grid
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                RowSpacing = 10,
                ColumnSpacing = 200
            };

            grid.Children.Add(this.CreateNewRosterImage(), 0, 0);
            grid.Children.Add(PlaceHolder(), 0, 1);
            grid.Children.Add(PlaceHolder(), 0, 2);
            grid.Children.Add(this.CreateNewAddEmployee(), 1, 0);
            grid.Children.Add(PlaceHolder(), 1, 1);
            grid.Children.Add(PlaceHolder(), 1, 2);
            grid.Children.Add(PlaceHolder(), 2, 0);
            grid.Children.Add(PlaceHolder(), 2, 1);
            grid.Children.Add(PlaceHolder(), 2, 2);
            return grid;
        }

        private View PhoneGrid()
        {
            var grid = new Grid
            {
                RowSpacing = 10,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            grid.Children.Add(this.CreateNewRosterImage(), 0, 0);
            grid.Children.Add(PlaceHolder(), 0, 1);
            grid.Children.Add(PlaceHolder(), 0, 2);
            grid.Children.Add(this.CreateNewAddEmployee(), 1, 0);
            grid.Children.Add(PlaceHolder(), 1, 1);
            grid.Children.Add(PlaceHolder(), 1, 2);
            return grid;
        }

        private StackLayout PlaceHolder()
        {
            Image newRoster = this.GetImage(AppStringResources.PlaceHolder);
            newRoster.Margin = 10;
            return CreateStack("Place Holder", newRoster);
        }

        private StackLayout CreateNewRosterImage()
        {
            Image newRoster = this.GetImage(AppStringResources.CreateRosterFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.CreateNewRoster),
                NumberOfTapsRequired = 1
            };

            newRoster.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack("Create roster", newRoster);
        }

        private StackLayout CreateNewAddEmployee()
        {
            Image addEmployeeImage = this.GetImage(AppStringResources.AddFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.AddEmployees),
                NumberOfTapsRequired = 1
            };

            addEmployeeImage.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack("Add employees", addEmployeeImage);
        }

        private StackLayout CreateStack(string labelText, Image image)
        {
            Label label = new CustomLabelForm(labelText);
            label.HorizontalTextAlignment = TextAlignment.Center;

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = 10,
                Children =
            {
                image,
                label
            }
            };
        }

        private async void CreateNewRoster()
        {
            await Navigation.PushAsync(new CreateRosterPage(this.UserViewModel,null,null));
        }

        private async void AddEmployees()
        {
            await Navigation.PushAsync(new AddEmployeesPage(this.UserViewModel));
        }


        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource
            };

        }
    }
}
