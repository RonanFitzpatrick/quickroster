﻿namespace QuickRoster.App.Views
{
    using System;
    using System.Net;
    using Behaviors;
    using Components;
    using Resources;
    using Services;
    using ViewModels;
    using Xamarin.Forms;

    public class AddEmployeesPage : ContentPage
    {

        private Entry Email;
        private Entry Name;
        private Label EmailErrorLabel;
        private Label NameErrorLabel;
        private ErrorLabel SubmitErrorLabel;
        private CustomGreenButton RegisterButton;
        private UserViewModel UserViewModel;

        public AddEmployeesPage(UserViewModel userViewModel)
        {
            this.Title = AppStringResources.AddUser;
            this.BackgroundColor = Color.White;
            this.UserViewModel = userViewModel;

            SetContent();
        }

        private void SetContent()
        {
            this.SetupComponents();

            this.Content = new StackLayout
            {

                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,

                Children =
                {
                    new CustomLabelForm(AppStringResources.EmployeeEmail),
                    this.Email,
                    this.EmailErrorLabel,
                    this.RegisterButton,
                    this.SubmitErrorLabel
                }
            };
        }

        private void SetupComponents()
        {
            this.EmailErrorLabel = new ErrorLabel();
            this.SubmitErrorLabel = new ErrorLabel();

            this.Email = new Entry
            {
                Placeholder = AppStringResources.Email,
                Behaviors = { new EmailValidatorBehavior(this.EmailErrorLabel) },
            };

            this.RegisterButton = new CustomGreenButton(AppStringResources.AddUser);
            this.RegisterButton.Clicked += this.AddEmployeeBusiness;
        }

        private async void AddEmployeeBusiness(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(this.Email.Text))
            {
                this.SubmitErrorLabel.Text = string.Empty;
                this.RegisterButton.IsEnabled = false;
                var response = await this.UserViewModel.CreateAccount(this.Email.Text);

                if (response == HttpStatusCode.Created || response == HttpStatusCode.OK)
                {
                    SubmitErrorLabel.SuccessMessage(AppStringResources.EmployeeAdded);
                }
                else
                {
                    SubmitErrorLabel.ErrorMessage(AppStringResources.Invalid);
                }
                this.RegisterButton.IsEnabled = true;
                return;
            }
            SubmitErrorLabel.ErrorMessage(AppStringResources.Invalid);
        }
    }
}
