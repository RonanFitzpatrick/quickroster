﻿namespace QuickRoster.App.Views
{
    using System.Collections.Generic;
    using System.Linq;
    using Enums;
    using Models.DTO;
    using Resources;
    using Xamarin.Forms;


    class RosterPage : ContentPage
    {
        private RosterDTO Roster { get; set; }
        public RosterPage(RosterDTO roster)
        {
            this.Roster = roster;
            this.Title = AppStringResources.Roster;
            this.BackgroundColor = Color.White;
            SetContent();
        }

        private void SetContent()
        {
            var scroll = new ScrollView();
            Content = scroll;
            var stack = new StackLayout();
            scroll.Content = stack;
            var groupedShifts = this.Roster.Shifts.GroupBy(x => x.Name);

            foreach (var group in groupedShifts)
            {
                List<ShiftDTO> shifts = group.AsEnumerable().ToList();
                var orderedShifts = shifts.OrderBy(x => x.Day);

                Grid grid = new Grid();
                Label name = new Label();
                name.Text = shifts[0].Name;

                Label dayTag = new Label();
                dayTag.Text = AppStringResources.Day;

                Label skillTag = new Label();
                skillTag.Text = AppStringResources.Skill;

                Label startTimeTag = new Label();
                startTimeTag.Text = AppStringResources.StartTime;


                Label endTimeTag = new Label();
                endTimeTag.Text = AppStringResources.FinishTime;

                grid.Children.Add(name, 0,0);
                grid.Children.Add(dayTag, 1, 1);
                grid.Children.Add(skillTag, 2, 1);
                grid.Children.Add(startTimeTag, 3, 1);
                grid.Children.Add(endTimeTag, 4, 1);
                int count = 2;

                foreach (var shift in orderedShifts)
                {
                    Label day = new Label();
                    day.Text = GetDayString((Day)shift.Day);

                    Label skill = new Label();
                    skill.Text = shift.Skill;

                    Label startTime = new Label();
                    startTime.Text = shift.StartTime;


                    Label endTime = new Label();
                    endTime.Text = shift.EndTime;

                    grid.Children.Add(day, 1, count);
                    grid.Children.Add(skill, 2, count);
                    grid.Children.Add(startTime, 3, count);
                    grid.Children.Add(endTime, 4, count);
                    count++;

                }
                stack.Children.Add(grid);
            }

        }


        private string GetDayString(Day shiftDay)
        {
            switch (shiftDay)
            {
                case Enums.Day.Monday:
                    return AppStringResources.Monday;
                case Enums.Day.Tuesday:
                    return AppStringResources.Tuesday;
                case Enums.Day.Wednesday:
                    return AppStringResources.Wednesday;
                case Enums.Day.Thursday:
                    return AppStringResources.Thursday;
                case Enums.Day.Friday:
                    return AppStringResources.Friday;
                case Enums.Day.Saturday:
                    return AppStringResources.Saturday;
                case Enums.Day.Sunday:
                    return AppStringResources.Sunday;
            }
            return "";
        }
    }
}
