﻿namespace QuickRoster.App.Views
{
    using Components;
    using Resources;
    using ViewModels;
    using System.Collections.Generic;
    using Xamarin.Forms;

    public class EmployeeHomePage : ContentPage
    {
        private UserViewModel UserViewModel;
        public EmployeeHomePage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.Home;
            this.BackgroundColor = Color.White;
            this.SetContent();
        }

        private void SetContent()
        {
            var grid = new Grid
            {
                RowSpacing = 10,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            grid.Children.Add(this.ViewRosterImage(), 0, 0);
            grid.Children.Add(PlaceHolder(), 0, 1);
            grid.Children.Add(PlaceHolder(), 0, 2);
            grid.Children.Add(this.EditAvailabilityImage(), 1, 0);
            grid.Children.Add(PlaceHolder(), 1, 1);
            grid.Children.Add(PlaceHolder(), 1, 2);
            this.Content =  grid;
        }

        private StackLayout PlaceHolder()
        {
            Image newRoster = this.GetImage(AppStringResources.PlaceHolder);
            newRoster.Margin = 10;
            return CreateStack("Place Holder", newRoster);
        }


        private StackLayout ViewRosterImage()
        {
            Image newRoster = this.GetImage(AppStringResources.CreateRosterFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.ViewRosterPage),
                NumberOfTapsRequired = 1
            };

            newRoster.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack(AppStringResources.ViewRoster, newRoster);
        }

        private StackLayout EditAvailabilityImage()
        {
            Image addEmployeeImage = this.GetImage(AppStringResources.EditFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.EditAvailibility),
                NumberOfTapsRequired = 1
            };

            addEmployeeImage.GestureRecognizers.Add(tapGestureRecognizer);

            return CreateStack(AppStringResources.EditAvailibility, addEmployeeImage);
        }

        private StackLayout CreateStack(string labelText, Image image)
        {
            Label label = new CustomLabelForm(labelText);
            label.HorizontalTextAlignment = TextAlignment.Center;

            return new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Margin = 10,
                Children =
            {
                image,
                label
            }
            };
        }

        private async void ViewRosterPage()
        {
            IEnumerable<Models.DTO.RosterDTO> rosters = await this.UserViewModel.GetRosters();
            await Navigation.PushAsync(new ViewRostersPage(this.UserViewModel, rosters));
        }

        private async void EditAvailibility()
        {
            await Navigation.PushAsync(new EditAvailibilityPage(this.UserViewModel));
        }


        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource
            };

        }
    }
}
