﻿namespace QuickRoster.App.Views
{
    using System.Collections.ObjectModel;
    using Components;
    using Resources;
    using ViewModels;
    using System.Collections.Generic;
    using Models;
    using Models.DTO;
    using Xamarin.Forms;
    using Image = Xamarin.Forms.Image;

    public class EditAvailibilityPage : ContentPage
    {
        private UserViewModel UserViewModel;
        private List<Day> DayList;
        private List<string> TimeList;
        private Dictionary<string, List<Switch>> SwitchMap;

        public EditAvailibilityPage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.EditAvailibility;
            this.BackgroundColor = Color.White;
            var scroll = new ScrollView();
            Content = scroll;
            StackLayout stack = new StackLayout();
            scroll.Content = stack;
            this.SetMenu(stack);
            this.SetGrid(stack);
        }

        private void SetMenu(StackLayout stack)
        {
            Grid grid = new Grid();
            grid.BackgroundColor = Color.Black;
            grid.HorizontalOptions = LayoutOptions.Center;
            grid.Children.Add(new Label {Text = AppStringResources.SaveChanges, TextColor =  Color.White, VerticalOptions = LayoutOptions.Center, FontSize = 12}, 5, 0);
            grid.Children.Add(SaveChangesImage(), 6,0);
            stack.Children.Add(grid);

        }

        private void SetGrid(StackLayout stack)
        {
            Grid grid = new Grid();
            grid.HorizontalOptions = LayoutOptions.Center;
            grid.ColumnSpacing = 1;
            grid.Children.Add(new CustomLabelTimes(AppStringResources.Time), 0, 0);

            this.DayList = new List<Day>
            {
                new Day {ShortHand = AppStringResources.MondayShorthand, LongHand = AppStringResources.Monday},
                new Day {ShortHand = AppStringResources.TuesdayShorthand, LongHand = AppStringResources.Tuesday},
                new Day {ShortHand = AppStringResources.WednesdayShorthand, LongHand = AppStringResources.Wednesday},
                new Day {ShortHand = AppStringResources.ThursdayShorthand, LongHand = AppStringResources.Thursday},
                new Day {ShortHand = AppStringResources.FridayShorthand, LongHand = AppStringResources.Friday},
                new Day {ShortHand = AppStringResources.SaturdayShorthand, LongHand = AppStringResources.Saturday},
                new Day {ShortHand = AppStringResources.SundayShorthand, LongHand = AppStringResources.Sunday},
            };

            this.TimeList = GetTimeList();

            int dayPosition = 1;

            this.SwitchMap = new Dictionary<string,List<Switch>>();

            foreach(var day in this.DayList)
            {
                grid.Children.Add(new CustomLabelTimes(day.ShortHand), dayPosition, 0);
                this.SwitchMap.Add(day.ShortHand,new List<Switch>());

                for (int i = 1; i < 25; i++)
                {
                    CustomAvailibilitySwitch currentSwitch = new CustomAvailibilitySwitch(i, dayPosition);
                    this.SwitchMap[day.ShortHand].Add(currentSwitch);
                    grid.Children.Add(currentSwitch,dayPosition,i);
                }

                dayPosition++;
            }

            int timePosition = 1;

            foreach (var time in this.TimeList)
            {
                grid.Children.Add(new CustomLabelTimes(time), 0, timePosition);
                timePosition++;
            }

            stack.Children.Add(grid);
        }

        private List<string> GetTimeList()
        {
            List<string> list = new List<string>();

            AddTimes(AppStringResources.AM, list);
            AddTimes(AppStringResources.PM, list);

            return list;
        }

        private void AddTimes(string postfix, List<string> list)
        {
            list.Add(12 + postfix);
            for (int i = 1; i <= 11; i++)
            {
                list.Add(i + postfix);
            }
        }

        private Image SaveChangesImage()
        {
            Image saveAvailibilityImg = this.GetImage(AppStringResources.SaveFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.SaveAvailibility),
                NumberOfTapsRequired = 1
            };

            saveAvailibilityImg.GestureRecognizers.Add(tapGestureRecognizer);

            return saveAvailibilityImg;
        }

        private async void SaveAvailibility()
        {
            Collection<Availibility> availibilities = CreateAvailibilityList();
            await this.UserViewModel.EditAvailibility(availibilities);
        }

        private Collection<Availibility> CreateAvailibilityList()
        {
            Collection<Availibility> availibilities = new Collection<Availibility>();
            foreach (var day in this.DayList)
            {
                Availibility availibility = new Availibility();
                availibility.Day = day.LongHand;
                availibility.TwelveAm = this.SwitchMap[day.ShortHand][0].IsToggled;
                availibility.OneAm = this.SwitchMap[day.ShortHand][1].IsToggled;
                availibility.TwoAm = this.SwitchMap[day.ShortHand][2].IsToggled;
                availibility.ThreeAm = this.SwitchMap[day.ShortHand][3].IsToggled;
                availibility.FourAm = this.SwitchMap[day.ShortHand][4].IsToggled;
                availibility.FiveAm = this.SwitchMap[day.ShortHand][5].IsToggled;
                availibility.SixAm = this.SwitchMap[day.ShortHand][6].IsToggled;
                availibility.SevenAm = this.SwitchMap[day.ShortHand][7].IsToggled;
                availibility.EightAm = this.SwitchMap[day.ShortHand][8].IsToggled;
                availibility.NineAm = this.SwitchMap[day.ShortHand][9].IsToggled;
                availibility.TenAm = this.SwitchMap[day.ShortHand][10].IsToggled;
                availibility.ElevenAm = this.SwitchMap[day.ShortHand][11].IsToggled;
                availibility.TwelvePm = this.SwitchMap[day.ShortHand][12].IsToggled;
                availibility.OnePm = this.SwitchMap[day.ShortHand][13].IsToggled;
                availibility.TwoPm = this.SwitchMap[day.ShortHand][14].IsToggled;
                availibility.ThreePm = this.SwitchMap[day.ShortHand][15].IsToggled;
                availibility.FourPm = this.SwitchMap[day.ShortHand][16].IsToggled;
                availibility.FivePm = this.SwitchMap[day.ShortHand][17].IsToggled;
                availibility.SixPm = this.SwitchMap[day.ShortHand][18].IsToggled;
                availibility.SevenPm = this.SwitchMap[day.ShortHand][19].IsToggled;
                availibility.EightPm = this.SwitchMap[day.ShortHand][20].IsToggled;
                availibility.NinePm = this.SwitchMap[day.ShortHand][21].IsToggled;
                availibility.TenPm = this.SwitchMap[day.ShortHand][22].IsToggled;
                availibility.ElevenPm = this.SwitchMap[day.ShortHand][23].IsToggled;
                availibilities.Add(availibility);
            }
            return availibilities;
        }

        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource,
                Margin = new Thickness(0,0,10,0)
            };

        }
    }
}
