﻿namespace QuickRoster.App.Views
{
    using System.Collections.Generic;
    using Components;
    using Models;
    using Models.DTO;
    using Xamarin.Forms;
    using Resources;
    using ViewModels;
    using Image = Xamarin.Forms.Image;

    public class CreateTemplatePage : ContentPage
    {
        private UserViewModel UserViewModel;
        private StackLayout Stack;
        private StackLayout ShiftGrid;
        private List<Models.Shift> ShiftList;
        private Entry TemplateName;

        public CreateTemplatePage(UserViewModel userViewModel)
        {
            this.UserViewModel = userViewModel;
            this.Title = AppStringResources.NewTemplate;
            this.BackgroundColor = Color.White;
            this.ShiftList = new List<Models.Shift>();

            var scroll = new ScrollView();
            Content = scroll;

            this.ShiftGrid = new StackLayout();
            var menu = this.GetMenuGrid();
            this.AddShift();
            CustomLabelForm label = new CustomLabelForm(AppStringResources.AddShift);
            label.HorizontalOptions = LayoutOptions.Center;
            this.Stack = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                Children =
                {
                    menu,
                    this.ShiftGrid,
                    AddShiftImage(),
                    label
                }
            };

            scroll.Content = this.Stack;
        }


        private Grid GetMenuGrid()
        {
            Grid menuGrid = new Grid();
            menuGrid.BackgroundColor = Color.FromHex("#D3D3D3");
            this.TemplateName = new Entry
            {
                Placeholder = AppStringResources.TemplateName,
                BackgroundColor = Color.FromHex("#D3D3D3")
            };

            menuGrid.Children.Add(new CustomLabelForm(AppStringResources.TemplateName),0,0);
            menuGrid.Children.Add(this.TemplateName, 1, 0);
            menuGrid.Children.Add(new CustomLabelForm(AppStringResources.SaveChanges), 5, 0);
            menuGrid.Children.Add(SaveChangesImage(), 6, 0);
            return menuGrid;
        }

        private Image SaveChangesImage()
        {
            Image saveTemplateImg = this.GetImage(AppStringResources.SaveFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.SaveTemplate),
                NumberOfTapsRequired = 1
            };

            saveTemplateImg.GestureRecognizers.Add(tapGestureRecognizer);

            return saveTemplateImg;
        }

        private async void SaveTemplate()
        {
            RosterTemplate template = new RosterTemplate
            {
                TemplateName = this.TemplateName.Text,
                TemplateShifts = new List<ShiftTemplate>()
            };

            foreach (var shift in this.ShiftList)
            {
                ShiftTemplate shiftTemplate = new ShiftTemplate
                {
                    Day = (Enums.Day) shift.Day.SelectedIndex,
                    StartHour = shift.StartTime.Items[shift.StartTime.SelectedIndex],
                    FinishHour = shift.EndTime.Items[shift.EndTime.SelectedIndex],
                    SkillRequired = new Skill {SkillName = shift.Skill.Items[shift.Skill.SelectedIndex]}
                };

                template.TemplateShifts.Add(shiftTemplate);


            }
            await this.UserViewModel.CreateTemplate(template);
        }


        private Image AddShiftImage()
        {
            Image addShiftImage = this.GetImage(AppStringResources.AddFileName);

            var tapGestureRecognizer = new TapGestureRecognizer
            {
                Command = new Command(this.AddShift),
                NumberOfTapsRequired = 1
            };

            addShiftImage.GestureRecognizers.Add(tapGestureRecognizer);
            addShiftImage.HorizontalOptions = LayoutOptions.Center;
            return addShiftImage;
        }

        private async void AddShift()
        {
            Grid shift = new Grid();
            Picker startTime = this.GetTimePicker();
            Picker endTime = this.GetTimePicker();
            Picker skillPicker = this.GetSkillPicker();
            Picker dayPicker = this.GetDayPicker();

            this.ShiftList.Add(new Models.Shift
            {
                Day = dayPicker,
                Skill = skillPicker,
                StartTime = startTime,
                EndTime = endTime
            });

            shift.Children.Add(new CustomLabelForm("Day"),0,0);
            shift.Children.Add(dayPicker, 0, 1);
            shift.Children.Add(startTime, 1, 1);
            shift.Children.Add(endTime, 2, 1);
            shift.Children.Add(new CustomLabelForm("Start Time"), 1, 0);
            shift.Children.Add(new CustomLabelForm("End Time"), 2,0);
            shift.Children.Add(new CustomLabelForm("Skill"), 3, 0);
            shift.Children.Add(skillPicker, 3, 1);
            this.ShiftGrid.Children.Add(shift);
        }

        public Picker GetTimePicker()
        {
            Picker timePicker = new Picker();

            AddTimes(timePicker,AppStringResources.AM);
            AddTimes(timePicker, AppStringResources.PM);

            return timePicker;
        }

        private void AddTimes(Picker timePicker, string postfix)
        {
            timePicker.Items.Add(12 + postfix);
            for (int i = 1; i <= 11; i++)
            {
                timePicker.Items.Add(i + postfix);
            }
        }

        private void AddTimes(string postfix, List<string> list)
        {
            list.Add(12 + postfix);
            for (int i = 1; i <= 11; i++)
            {
                list.Add(i + postfix);
            }
        }


        public Picker GetDayPicker()
        {
            Picker dayPicker = new Picker();

            dayPicker.Items.Add(AppStringResources.Monday);
            dayPicker.Items.Add(AppStringResources.Tuesday);
            dayPicker.Items.Add(AppStringResources.Wednesday);
            dayPicker.Items.Add(AppStringResources.Thursday);
            dayPicker.Items.Add(AppStringResources.Friday);
            dayPicker.Items.Add(AppStringResources.Saturday);
            dayPicker.Items.Add(AppStringResources.Sunday);

            return dayPicker;
        }

        public Picker GetSkillPicker()
        {
            Picker skillPicker = new Picker();

            //todo call api to get these
            skillPicker.Items.Add("Deli");
            skillPicker.Items.Add("Supervisor");
            skillPicker.Items.Add("Cashier");

            return skillPicker;
        }


        private Image GetImage(string fileName)
        {
            ImageSource imageSource = ImageSource.FromFile(
                  Device.OnPlatform(
                      fileName,
                      fileName,
                      fileName));

            return new Image
            {
                Aspect = Aspect.AspectFit,
                Source = imageSource,
                Margin = new Thickness(0, 0, 10, 0)
            };

        }
    }
}