﻿namespace QuickRoster.App.ViewModels
{
    using System.Threading.Tasks;
    using QuickRoster.App.Models;
    using QuickRoster.App.Services;

    public class GoogleUserViewModel
    {
        public GoogleUser User { get; set; }

        private readonly GoogleServices _googleServices;

        public GoogleUserViewModel()
        {
            _googleServices = new GoogleServices();
        }

        public async Task<string> GetAccessTokenAsync(string code)
        {
            var accessToken = await _googleServices.GetAccessTokenAsync(code);

            return accessToken;
        }

        public async Task SetGoogleUserProfileAsync(string accessToken)
        {
            User = await _googleServices.GetGoogleUserProfileAsync(accessToken);
        }
    }
}
