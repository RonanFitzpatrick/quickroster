﻿namespace QuickRoster.App.ViewModels
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Enums;
    using Models;
    using Models.DTO;
    using Services;
    using Views;
    using Xamarin.Forms;
    using Shift = Models.DTO.Shift;
    using User = Models.User;


    public class UserViewModel
    {
        public User User { get; set; }

        private readonly AccountService AccountServices;
        private readonly EmployerService EmployerService;

        public UserViewModel()
        {
            this.AccountServices = new AccountService();
            this.EmployerService = new EmployerService(this);
        }

        public async Task SetUser(GoogleUser googleUser)
        {
            User =  await AccountServices.Authenticate(googleUser);
        }

        public async Task<HttpStatusCode> CreateAccount(string email)
        {
            return await this.AccountServices.CreateAccount(this.User,email);
        }

        public async Task<HttpStatusCode> JoinBusiness(string code)
        {
            return await this.AccountServices.JoinBusiness(this.User, code);
        }

        public async Task<HttpStatusCode> EditAvailibility(ICollection<Availibility> availibilities )
        {
            return await this.AccountServices.EditAvailibility(this.User, availibilities);
        }

        public async Task<HttpStatusCode> CreateTemplate(RosterTemplate template)
        {
            return await this.EmployerService.CreateTemplate(template);
        }


        public ContentPage GetHomePage()
        {
            if (this.User.MemberOfBusiness)
            {
                return  Device.Idiom == TargetIdiom.Phone ? new EmployeeHomePage(this) : this.GetpageForRole();
            }

            return new RegisterPage(this);
        }

        private ContentPage GetpageForRole()
        {
            switch (this.User.Role)
            {
                case RoleEnum.Admin:
                    return new EmployerHomePage(this);
                case RoleEnum.User:
                    return new EmployeeHomePage(this);
                default:
                    return new RegisterPage(this);
            }
        }

        public async Task<IEnumerable<string>> GetRosterTemplateList()
        {
            return await this.EmployerService.GetTemplateNames();
        }


        public async Task<IEnumerable<Models.DTO.User>> GetEmployeeList()
        {
            return await this.EmployerService.GetEmployees();
        }

        public async Task<RosterTemplate> GetRosterTemplate(string templateName)
        {
            return await this.EmployerService.GetRosterTemplate(templateName);
        }

        public async Task SaveRoster(List<Shift> assignedShifts)
        {
            await this.EmployerService.SaveRoster(assignedShifts);
        }

        public async Task<IEnumerable<RosterDTO>> GetRosters()
        {
            return await this.AccountServices.GetRosters(this.User.Email);
        }
    }
}
