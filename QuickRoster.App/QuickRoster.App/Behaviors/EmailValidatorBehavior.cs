﻿namespace QuickRoster.App.Behaviors
{
    using System;
    using System.Text.RegularExpressions;
    using Resources;
    using Samples;
    using Xamarin.Forms;

    public class EmailValidatorBehavior : Behavior<Entry>
    {
        private readonly string emailRegex = AppStringResources.EmailRegex;

        static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool),
            typeof(NumberValidatorBehavior), false);

        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;
        private Label errorLabel;

        public EmailValidatorBehavior(Label errorLabel) : base()
        {
            this.errorLabel = errorLabel;
        }
        public bool IsValid
        {
            get { return (bool) base.GetValue(IsValidProperty); }
            private set { base.SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
        }

        void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            IsValid =
                (Regex.IsMatch(e.NewTextValue, emailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            ((Entry) sender).TextColor = IsValid ? Color.Default : Color.Red;

            this.errorLabel.Text = this.IsValid ? string.Empty : AppStringResources.EmailInvalid;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;

        }
    }
}