﻿namespace QuickRoster.App.Components
{
    using Xamarin.Forms;


    public class ErrorLabel : Label
    {
        public ErrorLabel() : base()
        {
            this.FontSize = 15;
        }

        public void SuccessMessage(string message)
        {
            this.TextColor = Color.Green;
            this.Text = message;
        }

        public void ErrorMessage(string message)
        {
            this.TextColor = Color.Red;
            this.Text = message;
        }
    }
}
