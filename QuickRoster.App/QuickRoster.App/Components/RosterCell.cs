﻿namespace QuickRoster.App.Components
{
    using Models.DTO;
    using Resources;
    using Xamarin.Forms;


    public class RosterCell : ViewCell
    {
        public RosterDTO Roster { get; set; }

        public RosterCell(RosterDTO roster)
        {
            this.Roster = roster;
            StackLayout cellWrapper = new StackLayout();
            StackLayout horizontalLayout = new StackLayout();
            Label left = new Label();
            Label right = new Label();

            left.SetBinding(Label.TextProperty, "Week starting: ");
            right.SetBinding(Label.TextProperty, Roster.WeekStarting.Day +" / " + Roster.WeekStarting.Month + " / " + Roster.WeekStarting.Year);

            horizontalLayout.Orientation = StackOrientation.Horizontal;
            right.HorizontalOptions = LayoutOptions.EndAndExpand;
            left.TextColor = Color.FromHex(AppStringResources.ButtonColor);
            right.TextColor = Color.Black;

            horizontalLayout.Children.Add(left);
            horizontalLayout.Children.Add(right);
            cellWrapper.Children.Add(horizontalLayout);
            View = cellWrapper;
        }


    }
}
