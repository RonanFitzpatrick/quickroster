﻿namespace QuickRoster.App.Components
{
    using Xamarin.Forms;


    public class CustomLabelTimes : Label
    {
        public CustomLabelTimes(string text) : base()
        {
            this.TextColor = Color.Black;
            this.FontSize = 15;
            this.Text = text;
            this.HorizontalOptions = LayoutOptions.CenterAndExpand;
            this.VerticalOptions = LayoutOptions.CenterAndExpand;
        }
    }
}
