﻿namespace QuickRoster.App.Components
{
    using Enums;
    using Models.DTO;
    using Resources;
    using Xamarin.Forms;
    public class CustomShiftGrid : Grid
    {
        public CustomLabelForm StartTimeTag { get; set; }
        public CustomLabelForm DayTag { get; set; }
        public CustomLabelForm FinishTimeTag { get; set; }
        public CustomLabelForm SkillTag { get; set; }
        public CustomGreenButton Assign { get; set; }
        public CustomLabelForm StartTime { get; set; }
        public CustomLabelForm FinishTime { get; set; }
        public CustomLabelForm Day { get; set; }
        public CustomLabelForm Skill { get; set; }
        public Picker EmployeePicker { get; set; }

        public ShiftTemplate Template { get; set; }

        public CustomShiftGrid(ShiftTemplate template)
        {
            this.Template = template;
            this.StartTimeTag = new CustomLabelForm(AppStringResources.StartTime);
            this.DayTag = new CustomLabelForm(AppStringResources.Day);
            this.FinishTimeTag = new CustomLabelForm(AppStringResources.FinishTime);
            this.SkillTag = new CustomLabelForm(AppStringResources.Skill);
            this.Assign = new CustomGreenButton(AppStringResources.Assign);
            this.StartTime = new CustomLabelForm(Template.StartHour);
            this.FinishTime = new CustomLabelForm(Template.FinishHour);
            this.Day = new CustomLabelForm(GetDayString(Template.Day));
            this.Skill = new CustomLabelForm("Cashier");
            this.EmployeePicker = new Picker();
            this.Render();

        }

        public void Render()
        {
            this.Children.Add(this.DayTag, 0, 0);
            this.Children.Add(this.StartTimeTag, 1, 0);
            this.Children.Add(this.FinishTimeTag, 2, 0);
            this.Children.Add(this.SkillTag, 3, 0);
            this.Children.Add(this.Assign, 2, 3);
            this.Children.Add(Day, 0, 1);
            this.Children.Add(StartTime, 1, 1);
            this.Children.Add(FinishTime, 2, 1);
            this.Children.Add(Skill, 3, 1);
            this.Children.Add(EmployeePicker, 2, 2);
        }


        private string GetDayString(Day shiftDay)
        {
            switch (shiftDay)
            {
                case Enums.Day.Monday:
                    return AppStringResources.Monday;
                case Enums.Day.Tuesday:
                    return AppStringResources.Tuesday;
                case Enums.Day.Wednesday:
                    return AppStringResources.Wednesday;
                case Enums.Day.Thursday:
                    return AppStringResources.Thursday;
                case Enums.Day.Friday:
                    return AppStringResources.Friday;
                case Enums.Day.Saturday:
                    return AppStringResources.Saturday;
                case Enums.Day.Sunday:
                    return AppStringResources.Sunday;
            }
            return "";
        }
    }
}
