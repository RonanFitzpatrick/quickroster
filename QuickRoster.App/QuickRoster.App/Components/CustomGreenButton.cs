﻿namespace QuickRoster.App.Components
{
    using Resources;
    using Xamarin.Forms;

    public class CustomGreenButton : Button
    {
        public CustomGreenButton(string message) : base()
        {
            this.BackgroundColor = Color.FromHex(AppStringResources.ButtonColor);
            this.BorderColor = Color.Black;
            this.TextColor = Color.White;
            this.Text = message;
            this.FontSize = 26;
        }
    }
}
