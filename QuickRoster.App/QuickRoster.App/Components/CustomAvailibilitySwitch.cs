﻿namespace QuickRoster.App.Components
{
    using Xamarin.Forms;



    public class CustomAvailibilitySwitch : Switch
    {
        public int Row { get; set; }

        public int Column { get; set; }

        public CustomAvailibilitySwitch(int row, int column) : base()
        {
            this.Row = row;
            this.BackgroundColor = Color.Red;
            this.Column = column;
            this.Toggled += OnToggled;

        }

        private void OnToggled(object sender, ToggledEventArgs toggledEventArgs)
        {
            this.BackgroundColor = toggledEventArgs.Value ? Color.Green : Color.Red;
        }
    }
}
