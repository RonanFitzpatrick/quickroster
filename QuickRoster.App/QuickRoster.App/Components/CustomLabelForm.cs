﻿namespace QuickRoster.App.Components
{
    using Xamarin.Forms;


    public class CustomLabelForm : Label
    {
        public CustomLabelForm(string text) : base()
        {
            this.TextColor = Color.Black;
            this.FontSize = 20;
            this.Text = text;
        }
    }
}
