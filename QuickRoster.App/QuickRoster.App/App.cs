﻿namespace QuickRoster.App
{
    using QuickRoster.App.Views;
    using Resources;
    using Xamarin.Forms;

    public class App : Application
    {
        public App()
        {
            this.MainPage = new NavigationPage(new MainCsPage());
        }

        protected override void OnStart()
        {
            Current.Resources = new ResourceDictionary();
            var navigationStyle = new Style(typeof(NavigationPage));
            var barBackgroundColorSetter = new Setter { Property = NavigationPage.BarBackgroundColorProperty, Value = Color.Black};

            navigationStyle.Setters.Add(barBackgroundColorSetter);

            Current.Resources.Add(navigationStyle);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
