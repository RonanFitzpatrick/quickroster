﻿namespace QuickRoster.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Entities;
    using Enums;
    using Newtonsoft.Json;
    using Services.IServices;

    /// <summary>
    ///Controller for the employer functions
    /// </summary>
    [Route("api/[controller]")]
    public class EmployerController : Controller
    {

        private readonly IEmployerRepository EmployerRepository;
        private readonly IUserRepository UserRepository;
        private readonly IRosterTemplateRepository TemplateRepository;
        private readonly IRosterRepository RosterRepository;

        public EmployerController(IEmployerRepository employerRepository, IUserRepository userRepository, IRosterTemplateRepository templateRepository, IRosterRepository rosterRepository)
        {
            this.EmployerRepository = employerRepository;
            this.UserRepository = userRepository;
            this.TemplateRepository = templateRepository;
            this.RosterRepository = rosterRepository;
        }

        /// <summary>
        ///Registers an Employer
        /// </summary>
        /// <param name="employerEmail"> Employers Email address used for uniqueness</param>
        /// <param name="userEmail">The user who is setting up as an employers Email</param>
        /// <param name="employerName">The name of the employer or business</param>
        /// <returns>status code</returns>
        /// <response code="201">Business is created</response>
        /// <response code="409">If business already exists</response>
        /// <response code="400">If parameters are null or empty</response>
        /// <response code="401">If User requesting the add does not exist does not exist</response>
        [HttpGet("Register")]
        public async Task<IActionResult> Register(string employerEmail, string userEmail, string employerName)
        {

            if (string.IsNullOrEmpty(employerEmail) || string.IsNullOrEmpty(userEmail) ||
                string.IsNullOrEmpty(employerName))
            {
                return this.BadRequest();
            }

            User user = await GetUser(userEmail);

            if (user == null)
            {
                return this.Unauthorized();
            }

            if (await SetupBusiness(employerEmail, employerName, user))
            {
                return this.StatusCode(201);
            }

            return this.StatusCode(409);
        }


        /// <summary>
        /// Creates a Roster
        /// </summary>
        /// <param name="userEmail">The user who is setting up as an employers Email</param>
        /// <param name="assignedShifts">The JSON of the shifts assigned in the roster</param>
        /// <returns>status code</returns>
        /// <response code="201">Roster is created</response>
        /// <response code="404">If business or user does not exist</response>
        /// <response code="400">If parameters are null or empty</response>
        /// <response code="401">If user is not an admin</response>
        [HttpPost("Roster")]
        public async Task<IActionResult> Roster(string userEmail, [FromBody]List<Shift> assignedShifts)
        {
            if (string.IsNullOrEmpty(userEmail) || assignedShifts?.Count == 0)
            {
                return this.BadRequest();
            }

            var user = await this.UserRepository.GetByEmail(userEmail);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }
            if (user.Role.Value == (int)RoleEnum.User)
            {
                return this.Unauthorized();
            }

            DateTime nextMonday = GetMonday();
            Roster oldRoster = await this.RosterRepository.GetByDate(nextMonday);

            if (oldRoster != null)
            {
                await this.RosterRepository.DeleteAsync(oldRoster);
            }

            Roster roster = new Roster
            {
                EmployerId = user.EmployerId.Value,
                Shifts = assignedShifts,
                StartDate = GetMonday()
            };

            await this.RosterRepository.AddAsync(roster);
            return this.Ok();
        }

        private DateTime GetMonday()
        {
        DateTime tomorrow = DateTime.Today.AddDays(1);
        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)tomorrow.DayOfWeek + 7) % 7;
        return tomorrow.AddDays(daysUntilMonday);
        }


        /// <summary>
        ///Creates a roster template
        /// </summary>
        /// <param name="email">The users email who is creating a template</param>
        /// <param name="template">The json of the roster template object</param>
        /// <returns>status code</returns>
        /// <response code="201">Template is created</response>
        /// <response code="400">Email is null or invalid or template is null</response>
        /// <response code="404">User does not exist</response>
        /// <response code="401">User does not have permission</response>
        /// <response code="409">Template with same name exists already</response>
        [HttpPost("Template")]
        public async Task<IActionResult> Template(string email, [FromBody]RosterTemplate  template)
        {
            if (string.IsNullOrEmpty(email) || !Entities.User.ValidEmail(email) || template == null)
            {
                return this.BadRequest();
            }

            bool uniqueName = (await this.TemplateRepository.GetByName(template.TemplateName)) == null;
            var user = await this.UserRepository.GetByEmail(email);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }
            if (user.Role.Value == (int) RoleEnum.User)
            {
                return this.Unauthorized();
            }
            if (uniqueName)
            {
                template.EmployerId = user.EmployerId.Value;
                await this.TemplateRepository.AddAsync(template);
                return this.StatusCode(201);
            }

            return this.StatusCode(409);
        }

        /// <summary>
        ///Gets the list of template names associated with the business
        /// </summary>
        /// <param name="email">The users email who is requesting the template</param>
        /// <returns>status code</returns>
        /// <response code="400">Emails is null or invalid</response>
        /// <response code="404">user does not exist</response>
        /// <response code="401">User does not have permission</response>
        /// <response code="200">List of templates are returned</response>
        [HttpGet("TemplateNames")]
        public async Task<IActionResult> Template(string email)
        {
            if (string.IsNullOrEmpty(email) || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            var user = await this.UserRepository.GetByEmail(email);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }

            if (user.Role.Value == (int)RoleEnum.User)
            {
                return this.Unauthorized();
            }

            var templateNames = await this.TemplateRepository.GetRosterNamesForEmployer(user.EmployerId.Value);

            return this.Ok(templateNames);
        }


        /// <summary>
        ///Gets a template by name
        /// </summary>
        /// <param name="email">The users email who is requesting the template</param>
        /// <param name="templateName">The name of the roster looking to be gotten</param>
        /// <returns>status code</returns>
        /// <response code="400">Emails is null or invalid</response>
        /// <response code="404">user does not exist</response>
        /// <response code="401">User does not have permission</response>
        /// <response code="200">Template is returned</response>
        [HttpGet("Template")]
        public async Task<IActionResult> Template(string email, string templateName)
        {
            if (string.IsNullOrEmpty(email) || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            var user = await this.UserRepository.GetByEmail(email);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }

            if (user.Role.Value == (int)RoleEnum.User)
            {
                return this.Unauthorized();
            }

            var template = await this.TemplateRepository.GetRosterByName(templateName);
            return this.Ok(template);
        }

        /// <summary>
        ///Gets the employees of a business
        /// </summary>
        /// <param name="email">The users email who is requesting the list of employees</param>
        /// <returns>status code</returns>
        /// <response code="400">Emails is null or invalid</response>
        /// <response code="404">user does not exist</response>
        /// <response code="401">User does not have permission</response>
        /// <response code="200">List of users returned</response>
        [HttpGet("Employees")]
        public async Task<IActionResult> Employees(string email)
        {
            if (string.IsNullOrEmpty(email) || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            var user = await this.UserRepository.GetByEmail(email);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }

            if (user.Role.Value == (int)RoleEnum.User)
            {
                return this.Unauthorized();
            }

            var employees = await this.UserRepository.GetByBusinessId(user.EmployerId.Value);
            return this.Ok(employees);
        }

        private async Task<User> GetUser(string userEmail)
        {
            return await this.UserRepository.GetByEmail(userEmail);
        }

        private async Task<bool> SetupBusiness(string employerEmail, string employerName, User user)
        {
            Employer employer = await this.EmployerRepository.GetByEmail(employerEmail);

            user.Role = (int)RoleEnum.Admin;

            if (employer == null)
            {
                employer = new Employer
                {
                    Email = employerEmail,
                    Name = employerName,
                    Employees = new List<User> { user }
                };

                await this.EmployerRepository.AddAsync(employer);
                await this.UserRepository.UpdateAsync(user);
                return true;
            }
            return false;
        }


    }
}
