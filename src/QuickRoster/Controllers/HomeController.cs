﻿namespace QuickRoster.API.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    [Route("Loading")]
    public class HomeController : Controller
    {
        /// <summary>
        ///Basic loading page for app to use as redirectUrl
        /// </summary>
        /// <returns>Loading page</returns>
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
