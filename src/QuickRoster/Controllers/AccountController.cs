﻿namespace QuickRoster.API.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Castle.Core.Internal;
    using DTO;
    using Entities;
    using Enums;
    using Microsoft.AspNetCore.Mvc;
    using Services;
    using Services.IServices;

    /// <summary>
    ///The controller to manage regular users of the system
    /// </summary>
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IUserRepository UserRepository;
        private readonly IEmployerRepository EmployerRepository;
        private readonly IAvailibilityRepository AvailbilityRepository;
        private readonly IRosterRepository RosterRepository;
        private readonly IShiftTemplateRepository ShiftTemplateRepository;

        public AccountController(IUserRepository userRepository, IEmployerRepository employerRepository,
            IAvailibilityRepository availbilityRepository, IRosterRepository rosterRepository, IShiftTemplateRepository shiftTemplateRepository)
        {
            this.UserRepository = userRepository;
            this.EmployerRepository = employerRepository;
            this.AvailbilityRepository = availbilityRepository;
            this.RosterRepository = rosterRepository;
            this.ShiftTemplateRepository = shiftTemplateRepository;
        }

        /// <summary>
        ///Logs a user in or registers them
        /// </summary>
        /// <param name="email">The users email address to login or register with</param>
        /// <param name="name">The users name</param>
        /// <returns>Result code and the users role if they have one</returns>
        /// <response code="201">User is created</response>
        /// <response code="409">If business already exists</response>
        /// <response code="400">If parameters are null, empty or invalid email</response>
        /// <response code="401">If User exists but it not part of a business</response>
        /// <response code="200">If User exists and is part of a business</response>
        [HttpGet("LoginandOrRegister")]
        public async Task<IActionResult> LoginandOrRegister(string email, string name)
        {
            if (email == null || name == null || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            User user = await this.UserRepository.GetByEmail(email);

            if (user == null)
            {
                user = new User(email, name);
                return await this.CreateUser(user);
            }

            if (user.EmployerId.HasValue)
            {
                return this.Ok(user.Role);
            }

            return this.Unauthorized();
        }

        /// <summary>
        ///Adds employees to a business
        /// </summary>
        /// <param name="employeeEmail">Employee's email that is to be added associate with the employer</param>
        /// <param name="managerEmail">The manager who is adding them email address</param>
        /// <returns>Status code</returns>
        /// <response code="201">User is created</response>
        /// <response code="409">If business already exists</response>
        /// <response code="400">If parameters are null, empty or invalid email</response>
        /// <response code="401">If user already has an employer</response>
        /// <response code="404">If user or their employer do not exist</response>
        /// <response code="200">If an existing user is associated with the employer</response>
        [HttpGet("CreateAccount")]
        public async Task<IActionResult> CreateAccount(string employeeEmail, string managerEmail)
        {
            if (string.IsNullOrWhiteSpace(managerEmail) || string.IsNullOrWhiteSpace(employeeEmail) ||
                !Entities.User.ValidEmail(employeeEmail))
            {
                return this.BadRequest();
            }

            User user = await this.UserRepository.GetByEmailWithEmployer(managerEmail);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }

            Employer employer = await this.EmployerRepository.GetAsync(user.EmployerId.Value);

            if (employer == null)
            {
                return this.NotFound();
            }

            JoiningCode code = new JoiningCode
            {
                Employer = employer,
            };

            code.GenerateCode();

            return await this.AssociateUserWithEmployer(code, employeeEmail);
        }

        /// <summary>
        /// Employee joining a business
        /// </summary>
        /// <param name="email">The user who is looking to join email address</param>
        /// <param name="code">The code generated when the user is added by their employer</param>
        /// <returns>Status code and users role if they have one</returns>
        /// <response code="409">If business already exists</response>
        /// <response code="400">If parameters are null, empty or invalid email</response>
        /// <response code="401">If the code does not match the one generated</response>
        /// <response code="404">If user does not exist or does not have a joining code associated with them</response>
        /// <response code="200">If the user joins the business</response>
        [HttpGet("JoinBusiness")]
        public async Task<IActionResult> JoinBusiness(string email, string code)
        {
            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(code) || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            User user = await this.UserRepository.GetByEmailWithJoiningCode(email);

            if (user?.JoiningCode == null)
            {
                return this.NotFound();
            }

            if (user.EmployerId.HasValue)
            {
                return this.StatusCode(409);
            }

            if (code.Equals(user.JoiningCode.Code))
            {
                user.EmployerId = user.JoiningCode.EmployerId;
                user.Role = (int) RoleEnum.User;
                await this.UserRepository.UpdateAsync(user);
                return this.Ok(user.Role);
            }

            return this.Unauthorized();
        }

        /// <summary>
        ///Allows a user to edit their available working hours
        /// </summary>
        /// <param name="email">The users email address to login or register with</param>
        /// <param name="availibility">The users availibility</param>
        /// <returns>Result code</returns>
        /// <response code="400">If parameters are null, empty or invalid email</response>
        /// <response code="404">If user does not exist or availibility is null</response>
        /// <response code="200">If the users availibility is updated</response>
        [HttpPost("EditAvailibility")]
        public async Task<IActionResult> EditAvailibility(string email,
            [FromBody] ICollection<Availibility> availibility)
        {
            if (string.IsNullOrWhiteSpace(email) || !Entities.User.ValidEmail(email) || availibility == null)
            {
                return this.BadRequest();
            }

            User user = await this.UserRepository.GetByEmail(email);

            if (user?.Availibilities == null)
            {
                return this.NotFound();
            }

            await this.AvailbilityRepository.DeleteRangeByUserAsync(user.Id);

            foreach (var newAvailibility in availibility)
            {
                newAvailibility.UserId = user.Id;
                await this.AvailbilityRepository.AddAsync(newAvailibility);
            }

            return this.Ok();
        }


        private async Task<IActionResult> AssociateUserWithEmployer(JoiningCode code, string email)
        {
            User user = await this.UserRepository.GetByEmail(email);

            if (user == null)
            {
                user = new User
                {
                    Email = email,
                    JoiningCode = code,
                };
                return await this.CreateUser(user);
            }

            if (user.EmployerId.HasValue)
            {
                return this.Unauthorized();
            }

            user.JoiningCode = code;
            await this.UserRepository.UpdateAsync(user);
            return this.Ok();
        }

        private async Task<IActionResult> CreateUser(User user)
        {
            await this.UserRepository.AddAsync(user);
            return this.StatusCode(201);
        }


        /// <summary>
        /// Gets list of all rosters
        /// </summary>
        /// <param name="Email">The user who is setting up as an employers Email</param>
        /// <returns>status code</returns>
        /// <response code="200">Roster are returned</response>
        /// <response code="404">If user does not exist or no rosters are created</response>
        /// <response code="400">If email is null or empty</response>
        /// <response code="401">If user is not a member of a business</response>
        [HttpGet("Roster")]
        public async Task<IActionResult> Roster(string email)
        {
            if (string.IsNullOrWhiteSpace(email) || !Entities.User.ValidEmail(email))
            {
                return this.BadRequest();
            }

            User user = await this.UserRepository.GetByEmail(email);

            if (user?.EmployerId == null)
            {
                return this.NotFound();
            }

            IEnumerable<Roster> rosters = await this.RosterRepository.GetByBusinessIdWithShiftsAndTemplate(user.EmployerId.Value);

            IEnumerable<RosterDTO> dtos = await GetRosterDtos(rosters);

            return this.Ok(dtos);
        }


        private async Task<IEnumerable<RosterDTO>> GetRosterDtos(IEnumerable<Roster> rosters)
        {
            List<RosterDTO> dtos = new List<RosterDTO>();
            List<ShiftDTO> shiftDtos;
            ShiftDTO shiftDto;

            foreach (Roster roster in rosters)
            {
                shiftDtos = new List<ShiftDTO>();
                foreach (var shift in roster.Shifts)
                {
                    User user = await this.UserRepository.GetAsync(shift.UserID);
                    ShiftTemplate shiftTemplate = await this.ShiftTemplateRepository.GetAsync(shift.ShiftTemplateID);

                    shiftDto = new ShiftDTO
                    {
                        Name = user.Name,
                        Day = (int)shiftTemplate.Day,
                        EndTime = shiftTemplate.FinishHour,
                        StartTime = shiftTemplate.StartHour,
                        Skill = "Cashier"
                    };
                    shiftDtos.Add(shiftDto);
                }
                dtos.Add(new RosterDTO
                {
                    WeekStarting = roster.StartDate,
                    Shifts = shiftDtos
                });
            }

            return dtos;
        }
    }
}
