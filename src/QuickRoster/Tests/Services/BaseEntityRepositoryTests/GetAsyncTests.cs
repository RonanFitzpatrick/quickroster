﻿namespace QuickRoster.API.Tests.Services.BaseEntityRepositoryTests
{
    using System.Collections.Generic;
    using System.Linq;
    using API.Services;
    using Entities;
    using Microsoft.EntityFrameworkCore;
    using Xunit;
    using Moq;

    public class GetAsyncTests
    {
        private Mock<QuickRosterDbContext> dbMock;
        private Mock<DbSet<User>> dbSetMock;
        private BaseEntityRepository<User> repository;
        private User testUserOne;
        private User testUserTwo;
        //private BaseEntityRepository<dbsetMock> baseEntityRepository;
        public GetAsyncTests()
        {
            this.dbMock = new Mock<QuickRosterDbContext>();
            this.dbSetMock = new Mock<DbSet<User>>();
            this.testUserOne = new User("rf@gmail.com", "Ronan");
            this.testUserTwo = new User("rf@gmail.com", "Ronan");

            var data = new List<User>
            {
               testUserOne,
               testUserTwo
            }.AsQueryable();

            dbSetMock = new Mock<DbSet<User>>();

            dbSetMock.As<IQueryable<User>>().Setup(m => m.Provider).Returns(data.Provider);
            dbSetMock.As<IQueryable<User>>().Setup(m => m.Expression).Returns(data.Expression);
            dbSetMock.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(data.ElementType);
            dbSetMock.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            this.dbMock.Setup(x => x.Set<User>()).Returns(this.dbSetMock.Object);
           
            this.repository = new BaseEntityRepository<User>(this.dbMock.Object);
        }

        [Fact]
        public void ShouldReturnAListOfTwoUsersWhenCalled()
        {
            var result = this.repository.GetAsync();

            Assert.Equal(result.Result.FirstOrDefault(), this.testUserOne);
            Assert.Equal(result.Result.LastOrDefault(), this.testUserTwo);
        }
    }
}
