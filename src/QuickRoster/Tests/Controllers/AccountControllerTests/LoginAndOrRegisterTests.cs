﻿namespace QuickRoster.API.Tests.Controllers.AccountController
{
    using API.Controllers;
    using API.Services.IServices;
    using Entities;
    using Moq;
    using Xunit;
    using Microsoft.AspNetCore.Mvc;

    public class LoginAndOrRegisterTests
    {
        private AccountController Controller;
        private Mock<IUserRepository> MockUserRepository;
        private Mock<IEmployerRepository> MockEmployerRepository;
        private string MalformedEmail = "test@test";
        private string Email = "test@gmail.com";

        public LoginAndOrRegisterTests()
        {
            this.MockUserRepository = new Mock<IUserRepository>();
            this.MockEmployerRepository = new Mock<IEmployerRepository>();
            this.Controller = new AccountController(this.MockUserRepository.Object, this.MockEmployerRepository.Object,null,null,null);
        }

        [Fact]
        public async void ShouldCallAddWithCorrectParametersWhenGetReturnsNull()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.Email))))
                .ReturnsAsync((User) null);
            var expected = new StatusCodeResult(201);

            var result = await this.Controller.LoginandOrRegister(this.Email, "name");
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.MockUserRepository.Verify(
                x => x.AddAsync(It.Is<User>(y => y.Name.Equals("name") && y.Email.Equals(this.Email))), Times.Once);

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnStatusOKAndNotCallAddWhenGetReturnsUser()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.Email))))
                .ReturnsAsync(new User(this.Email, "name"));
            var expected = new UnauthorizedResult();

            var result = await this.Controller.LoginandOrRegister(this.Email, "name");
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenNameIsNull()
        {
            var expected = new BadRequestResult();

            var result = this.Controller.LoginandOrRegister(this.Email, null);
            var statusCode = await result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.LoginandOrRegister(null, "name");
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsInvalid()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.LoginandOrRegister(this.MalformedEmail, "name");
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

    }
}
