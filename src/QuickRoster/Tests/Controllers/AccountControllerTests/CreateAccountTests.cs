﻿namespace QuickRoster.API.Tests.Controllers.AccountControllerTests
{
    using API.Controllers;
    using API.Services.IServices;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Xunit;
    using Moq;
    public class CreateAccountTests
    {
        private AccountController Controller;
        private Mock<IUserRepository> MockUserRepository;
        private Mock<IEmployerRepository> MockEmployerRepository;
        private string EmployerEmail = "testEmployer@gmail.com";
        private int EmployerId = 1;
        private Employer employer;
        private string MalformedEmail = "test@test";
        private string Email = "test@gmail.com";
        private string Name = "employer";

        public CreateAccountTests()
        {
            this.employer = new Employer
            {
                Id = this.EmployerId,
                Name = this.Name,
                Email = this.Email
            };

            this.MockUserRepository = new Mock<IUserRepository>();
            this.MockEmployerRepository = new Mock<IEmployerRepository>();
            this.Controller = new AccountController(this.MockUserRepository.Object, this.MockEmployerRepository.Object,null,null,null);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenManagerEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.CreateAccount(this.Email, null);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmployeeEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.CreateAccount(null, this.EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequesIfEmailIsInvalid()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.CreateAccount(this.MalformedEmail, EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenManagerIsNotFound()
        {
            var expected = new NotFoundResult();
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
            .ReturnsAsync((User)null);

            var result = await this.Controller.CreateAccount(this.Email, EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmailWithEmployer(this.EmployerEmail), Times.Once);
            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenManagerDoesNotHaveABusiness()
        {
            var expected = new NotFoundResult();
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
             .ReturnsAsync(new User
             {
                 Id = 1,
                 EmployerId = null
             });


            var result = await this.Controller.CreateAccount(this.Email, EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmailWithEmployer(this.EmployerEmail), Times.Once);
            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenEmployerDoesNotExist()
        {
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
            .ReturnsAsync(new User
            {
                Id = 1,
                EmployerId = this.EmployerId
            });

            this.MockEmployerRepository.Setup(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)))
            .ReturnsAsync((Employer)null);

            var expected = new NotFoundResult();
            var result = await this.Controller.CreateAccount(this.Email, EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Never);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            this.MockEmployerRepository.Verify(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)), Times.Once);

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }


        [Fact]
        public async void ShouldCallAddWithCorrectParametersWhenUserReturnsNull()
        {
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
            .ReturnsAsync(new User
            {
                Id = 1,
                EmployerId = this.EmployerId
            });

            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.Email))))
                .ReturnsAsync((User)null);

            this.MockEmployerRepository.Setup(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)))
            .ReturnsAsync(employer);

            var expected = new StatusCodeResult(201);

            var result = await this.Controller.CreateAccount(this.Email, this.EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.MockUserRepository.Verify(
                x => x.AddAsync(It.Is<User>(y => y.Email.Equals(this.Email) && y.JoiningCode.Employer.Id == this.EmployerId && y.JoiningCode.Code.Length == 8)), Times.Once);
            this.MockEmployerRepository.Verify(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)), Times.Once);


            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnUnauthorizedWhenUserIsAlreadyRegisteredToAnEmployer()
        {
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
            .ReturnsAsync(new User
            {
                Id = 1,
                EmployerId = this.EmployerId
            });

            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.Email))))
                .ReturnsAsync(new User
                {
                    EmployerId =  this.EmployerId
                });

            this.MockEmployerRepository.Setup(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)))
            .ReturnsAsync(employer);

            var expected = new UnauthorizedResult();

            var result = await this.Controller.CreateAccount(this.Email, this.EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            this.MockEmployerRepository.Verify(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)), Times.Once);

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnOkWhenUserExistsButIsNotRegisteredToAnEmployer()
        {
            this.MockUserRepository.Setup(x => x.GetByEmailWithEmployer(It.Is<string>(y => y == this.EmployerEmail)))
            .ReturnsAsync(new User
            {
                Id = 1,
                EmployerId = this.EmployerId
            });

            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.Email))))
                .ReturnsAsync(new User
                {
                    Email = this.Email,
                    EmployerId = null
                });

            this.MockEmployerRepository.Setup(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)))
            .ReturnsAsync(employer);

            var expected = new OkResult();

            var result = await this.Controller.CreateAccount(this.Email, this.EmployerEmail);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.MockUserRepository.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Never);
            this.MockUserRepository.Verify
                (x => x.UpdateAsync(It.Is<User>(y => y.Email.Equals(this.Email) && y.JoiningCode.Employer.Id == this.EmployerId && y.JoiningCode.Code.Length == 8)), Times.Once);
            this.MockEmployerRepository.Verify(x => x.GetAsync(It.Is<int>(y => y == this.EmployerId)), Times.Once);

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }
    }
}
