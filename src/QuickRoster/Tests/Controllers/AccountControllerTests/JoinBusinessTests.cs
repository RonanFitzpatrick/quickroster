﻿namespace QuickRoster.API.Tests.Controllers.AccountControllerTests
{
    using API.Controllers;
    using API.Services.IServices;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Xunit;
    using Moq;
    public class JoinBusinessTests
    {

        private AccountController Controller;
        private Mock<IUserRepository> UserRepositoryMock;
        private Mock<IEmployerRepository> EmployerRepositroyMock;
        private string Email = "Test@gmail.com";
        private string MalformedEmail = "malfromed";
        private string Code = "12345678";


        public JoinBusinessTests()
        {
            this.UserRepositoryMock = new Mock<IUserRepository>();
            this.EmployerRepositroyMock = new Mock<IEmployerRepository>();
            this.Controller = new AccountController(this.UserRepositoryMock.Object, this.EmployerRepositroyMock.Object,null,null,null);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.JoinBusiness(null, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Never);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);

        }

        [Fact]
        public async void ShouldReturnBadRequestWhenCodeIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.JoinBusiness(this.Email,null);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Never);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsInvalid()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.JoinBusiness(this.MalformedEmail, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Never);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenUserDoesNotExist()
        {
            var expected = new NotFoundResult();
            this.UserRepositoryMock.Setup(x => x.GetByEmailWithJoiningCode(It.IsAny<string>())).ReturnsAsync((User)null);

            var result = await this.Controller.JoinBusiness(this.Email, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Once);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenUserDoesNotHaveAJoiningAssociatedWithThem()
        {
            var expected = new NotFoundResult();
            this.UserRepositoryMock.Setup(x => x.GetByEmailWithJoiningCode(It.IsAny<string>())).ReturnsAsync(new User
            {
                JoiningCode = null
            });

            var result = await this.Controller.JoinBusiness(this.Email, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Once);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnConflictWhenUserHasAnEmployer()
        {
            var expected = new StatusCodeResult(409);
            this.UserRepositoryMock.Setup(x => x.GetByEmailWithJoiningCode(It.IsAny<string>())).ReturnsAsync(new User
            {
                JoiningCode = new JoiningCode
                {
                    Code = this.Code,
                    EmployerId = 1,
                    UserId = 1
                },
                EmployerId = 1
            });

            var result = await this.Controller.JoinBusiness(this.Email, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Once);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnUnauthorizedWhenCodeDoesNotMatchServerCode()
        {
            var expected = new UnauthorizedResult();
            this.UserRepositoryMock.Setup(x => x.GetByEmailWithJoiningCode(It.IsAny<string>())).ReturnsAsync(new User
            {
                JoiningCode = new JoiningCode
                {
                    Code = "wrong code",
                    EmployerId = 1,
                    UserId = 1
                },
            });

            var result = await this.Controller.JoinBusiness(this.Email, this.Code);
            var statusCode = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Once);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<User>()), Times.Never);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }

        [Fact]
        public async void ShouldReturnOkWhenCodeMatchesServerCode()
        {
            User user = new User
            {
                Id = 1,
                JoiningCode = new JoiningCode
                {
                    Code = this.Code,
                    EmployerId = 1,
                    UserId = 1
                }
            };

            var expected = new OkObjectResult(0);
            this.UserRepositoryMock.Setup(x => x.GetByEmailWithJoiningCode(It.IsAny<string>())).ReturnsAsync(user);

            var result =  await this.Controller.JoinBusiness(this.Email, this.Code) as OkObjectResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmailWithJoiningCode(this.Email), Times.Once);
            this.UserRepositoryMock.Verify(x => x.UpdateAsync(It.Is<User>(y => y.Id == user.Id && y.EmployerId.Value == user.JoiningCode.EmployerId)), Times.Once);
            Assert.Equal(result.StatusCode, expected.StatusCode);
      }

    }
}
