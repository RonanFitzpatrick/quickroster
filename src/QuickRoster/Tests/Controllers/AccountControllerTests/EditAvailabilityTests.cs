﻿namespace QuickRoster.API.Tests.Controllers.AccountControllerTests
{
    using System.Collections.Generic;
    using API.Controllers;
    using API.Services;
    using API.Services.IServices;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    public class EditAvailabilityTests
    {
        private Mock<IUserRepository> UserRepositoryMock;
        private Mock<IEmployerRepository> EmployerRepositoryMock;
        private Mock<IAvailibilityRepository> AvailibilityRepositoryMock;
        private AccountController AccountController;
        private ICollection<Availibility> Availibilities;
        private string Email = "Test@gmail.com";
        private string MalformedEmail = "malfromed";

        public EditAvailabilityTests()
        {
            this.UserRepositoryMock = new Mock<IUserRepository>();
            this.AvailibilityRepositoryMock = new Mock<IAvailibilityRepository>();
            this.EmployerRepositoryMock = new Mock<IEmployerRepository>();
            this.AccountController = new AccountController(this.UserRepositoryMock.Object, this.EmployerRepositoryMock.Object, AvailibilityRepositoryMock.Object,null,null);
            this.Availibilities = new List<Availibility>
            {
                new Availibility
                {
                    Day = "Monday"
                },
                new Availibility
                {
                    Day = "Tuesday"
                },
                new Availibility
                {
                    Day = "Wednesday"
                },
                new Availibility
                {
                    Day = "Thursday"
                },
                new Availibility
                {
                    Day = "Friday"
                },
                new Availibility
                {
                    Day = "Saturday"
                },
                new Availibility
                {
                    Day = "Sunday"
                }
            };
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.AccountController.EditAvailibility(null, this.Availibilities);
            var actual = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmail(this.Email), Times.Never);
            Assert.Equal(expected.StatusCode, actual.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsInvalid()
        {
            var expected = new BadRequestResult();

            var result = await this.AccountController.EditAvailibility(this.MalformedEmail, this.Availibilities);
            var actual = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmail(this.Email), Times.Never);
            Assert.Equal(expected.StatusCode, actual.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenAvailibilityIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.AccountController.EditAvailibility(this.Email, null);
            var actual = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmail(this.Email), Times.Never);
            Assert.Equal(expected.StatusCode, actual.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundWhenUserDoesNotExist()
        {
            var expected = new NotFoundResult();

            this.UserRepositoryMock.Setup(x => x.GetByEmail(this.Email)).ReturnsAsync(null);

            var result = await this.AccountController.EditAvailibility(this.Email, this.Availibilities);
            var actual = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmail(this.Email), Times.Once);
            Assert.Equal(expected.StatusCode, actual.StatusCode);
        }

        [Fact]
        public async void ShouldReturnOkWhenUpdated()
        {
            var expected = new OkResult();
            this.UserRepositoryMock.Setup(x => x.GetByEmail(this.Email)).ReturnsAsync(new User {Id = 1});

            var result = await this.AccountController.EditAvailibility(this.Email, this.Availibilities);
            var actual = result as StatusCodeResult;

            this.UserRepositoryMock.Verify(x => x.GetByEmail(this.Email), Times.Once);
            this.AvailibilityRepositoryMock.Verify(x => x.DeleteRangeByUserAsync(It.Is<int>(y => y == 1)), Times.Once);

            Assert.Equal(expected.StatusCode, actual.StatusCode);
        }
    }
}
