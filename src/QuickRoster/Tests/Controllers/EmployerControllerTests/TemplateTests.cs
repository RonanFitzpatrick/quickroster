﻿namespace QuickRoster.API.Tests.Controllers.EmployerControllerTests
{
    using API.Controllers;
    using API.Services.IServices;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using API.Entities;
    using Enums;
    using Xunit;

    public class TemplateTests
    {

        private EmployerController Controller;
        private Mock<IUserRepository> MockUserRepository;
        private Mock<IEmployerRepository> MockEmployerRepository;
        private Mock<IRosterTemplateRepository> MockRosterTemplateRepository;
        private string UserEmail = "Employee@gmail.com";
        private string InvalidEmail = "INVALID";
        private RosterTemplate Template;

        public TemplateTests()
        {
            this.Template = new RosterTemplate
            {
                Id = 1,
                TemplateName = "test"
            };

            this.MockUserRepository = new Mock<IUserRepository>();
            this.MockEmployerRepository = new Mock<IEmployerRepository>();
            this.MockRosterTemplateRepository = new Mock<IRosterTemplateRepository>();
            this.Controller = new EmployerController(this.MockEmployerRepository.Object,
                this.MockUserRepository.Object, this.MockRosterTemplateRepository.Object,null);

        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.Template(null, this.Template);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode,statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenEmailIsInvalid()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.Template(InvalidEmail, this.Template);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnBadRequestWhenTemplateRosterIsNull()
        {
            var expected = new BadRequestResult();

            var result = await this.Controller.Template(this.UserEmail, (RosterTemplate)null);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnNotFoundIfUserDoesNotExist()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail)))).ReturnsAsync(null);
            this.MockRosterTemplateRepository.Setup(x => x.GetByName(It.Is<string>(y => y.Equals("test")))).ReturnsAsync(null);

            var expected = new NotFoundResult();

            var result = await this.Controller.Template(this.UserEmail, this.Template);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnUnauthorizedIfUserIsNotAnAdmin()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail))))
                .ReturnsAsync(new User
                {
                    Role = (int)RoleEnum.User,
                    EmployerId = 1
                });


            this.MockRosterTemplateRepository.Setup(x => x.GetByName(It.Is<string>(y => y.Equals("test")))).ReturnsAsync(null);

            var expected = new UnauthorizedResult();

            var result = await this.Controller.Template(this.UserEmail, this.Template);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnConflictWhenTemplateWithSameNameExists()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail))))
                .ReturnsAsync(new User
                {
                    Role = (int)RoleEnum.Admin,
                    EmployerId = 1
                });


            this.MockRosterTemplateRepository.Setup(x => x.GetByName(It.Is<string>(y => y.Equals("test")))).ReturnsAsync(this.Template);

            var expected = new StatusCodeResult(409);

            var result = await this.Controller.Template(this.UserEmail, this.Template);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Fact]
        public async void ShouldReturnCreatedWhenATemplateIsCreated()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail))))
               .ReturnsAsync(new User
               {
                   Role = (int)RoleEnum.Admin,
                   EmployerId = 1
               });


            this.MockRosterTemplateRepository.Setup(x => x.GetByName(It.Is<string>(y => y.Equals("test")))).ReturnsAsync(null);

            var expected = new StatusCodeResult(201);

            var result = await this.Controller.Template(this.UserEmail, this.Template);
            var statusCode = result as StatusCodeResult;

            this.MockRosterTemplateRepository.Verify(x => x.AddAsync(It.Is<RosterTemplate>(y => y.EmployerId == 1)), Times.Once);
            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }
    }
}