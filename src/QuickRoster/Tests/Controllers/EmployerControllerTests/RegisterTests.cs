﻿namespace QuickRoster.API.Tests.Controllers.EmployerController
{
    using System.Linq;
    using API.Controllers;
    using API.Services.IServices;
    using Entities;
    using Microsoft.AspNetCore.Mvc;
    using Xunit;
    using Moq;

    public class RegisterTests
    {

        private EmployerController Controller;
        private Mock<IUserRepository> MockUserRepository;
        private Mock<IEmployerRepository> MockEmployerRepository;
        private string EmployerEmail = "Employer@businees.com";
        private string UserEmail = "Employee@gmail.com";
        private string EmployerName = "Mr boss";

        public RegisterTests()
        {
            this.MockUserRepository = new Mock<IUserRepository>();
            this.MockEmployerRepository = new Mock<IEmployerRepository>();
            this.Controller = new EmployerController(this.MockEmployerRepository.Object,this.MockUserRepository.Object,null,null);
        }

        [Fact]
        public async void ShouldReturnUnauthorizedWhenUserDoesNotExist()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail)))).ReturnsAsync(null);
            var expected = new UnauthorizedResult();

            var result = await this.Controller.Register(this.EmployerEmail,this.UserEmail, this.EmployerName);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.UserEmail), Times.Once);
            Assert.Equal(expected.StatusCode, statusCode.StatusCode);
        }

        [Theory]
        [InlineData(null, "Employee@gmail.com", "Mr boss")]
        [InlineData("", "Employee@gmail.com", "Mr boss")]
        [InlineData("Employer@businees.com", null, "Mr boss")]
        [InlineData("Employer@businees.com", "", "Mr boss")]
        [InlineData("Employer@businees.com", "Employee@gmail.com", null)]
        [InlineData("Employer@businees.com", "Employee@gmail.com", "")]
        public async void ShouldReturnBadRequestWhenAnyArgumentIsNullOrEmpty(string employerEmail, string userEmail, string employerName)
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail)))).ReturnsAsync(null);
            var expected = new BadRequestResult();

            var result = await this.Controller.Register(employerEmail, userEmail, employerName);
            var statusCode = result as StatusCodeResult;

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }


        [Fact]
        public async void ShouldReturnConflictIfABusinessAlreadyExistsWithTheSameEmail()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail)))).ReturnsAsync(new User());
            this.MockEmployerRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.EmployerEmail)))).ReturnsAsync(new Employer());
            var expected = new StatusCodeResult(409);

            var result = await this.Controller.Register(this.EmployerEmail, this.UserEmail, this.EmployerName);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.UserEmail), Times.Once);
            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }


        [Fact]
        public async void ShouldReturnCreatedIfBusinessDoesNotExistWithTheSameEmail()
        {
            this.MockUserRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.UserEmail)))).ReturnsAsync(new User());
            this.MockEmployerRepository.Setup(x => x.GetByEmail(It.Is<string>(y => y.Equals(this.EmployerEmail)))).ReturnsAsync(null);
            var expected = new StatusCodeResult(201);

            var result = await this.Controller.Register(this.EmployerEmail, this.UserEmail, this.EmployerName);
            var statusCode = result as StatusCodeResult;

            this.MockUserRepository.Verify(x => x.GetByEmail(this.UserEmail), Times.Once);
            this.MockEmployerRepository.Verify(x => x.AddAsync(It.Is<Employer>(y => y.Name.Equals(this.EmployerName) && y.Email.Equals(this.EmployerEmail) && y.Employees.Count() == 1)), Times.Once);

            Assert.Equal(statusCode.StatusCode, expected.StatusCode);
        }
    }
}
