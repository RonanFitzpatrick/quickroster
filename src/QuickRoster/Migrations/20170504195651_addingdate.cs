﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class addingdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Roster_Employer_EmployerId",
                table: "Roster");

            migrationBuilder.AddColumn<int>(
                name: "TemplateId",
                table: "Shift",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EmployerId",
                table: "Roster",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "Roster",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Shift_TemplateId",
                table: "Shift",
                column: "TemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Roster_Employer_EmployerId",
                table: "Roster",
                column: "EmployerId",
                principalTable: "Employer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_ShiftTemplates_TemplateId",
                table: "Shift",
                column: "TemplateId",
                principalTable: "ShiftTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Roster_Employer_EmployerId",
                table: "Roster");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_ShiftTemplates_TemplateId",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_Shift_TemplateId",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "TemplateId",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "Roster");

            migrationBuilder.AlterColumn<int>(
                name: "EmployerId",
                table: "Roster",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Roster_Employer_EmployerId",
                table: "Roster",
                column: "EmployerId",
                principalTable: "Employer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
