﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class changingstartandfinishtrostrings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "StartHour",
                table: "ShiftTemplates",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<string>(
                name: "FinishHour",
                table: "ShiftTemplates",
                nullable: false,
                oldClrType: typeof(short));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "StartHour",
                table: "ShiftTemplates",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<short>(
                name: "FinishHour",
                table: "ShiftTemplates",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
