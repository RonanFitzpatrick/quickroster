﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace QuickRoster.API.Migrations
{
    public partial class addingAvailibility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Availibility",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<string>(nullable: true),
                    EightPm = table.Column<bool>(nullable: false),
                    ElevenAm = table.Column<bool>(nullable: false),
                    ElevenPm = table.Column<bool>(nullable: false),
                    FiveAm = table.Column<bool>(nullable: false),
                    FivePm = table.Column<bool>(nullable: false),
                    FourAm = table.Column<bool>(nullable: false),
                    FourPm = table.Column<bool>(nullable: false),
                    NineAm = table.Column<bool>(nullable: false),
                    NinePm = table.Column<bool>(nullable: false),
                    OneAm = table.Column<bool>(nullable: false),
                    OnePm = table.Column<bool>(nullable: false),
                    SevenAm = table.Column<bool>(nullable: false),
                    SevenPm = table.Column<bool>(nullable: false),
                    SixAm = table.Column<bool>(nullable: false),
                    SixPm = table.Column<bool>(nullable: false),
                    TenAm = table.Column<bool>(nullable: false),
                    TenPm = table.Column<bool>(nullable: false),
                    ThreeAm = table.Column<bool>(nullable: false),
                    Threem = table.Column<bool>(nullable: false),
                    TwelveAm = table.Column<bool>(nullable: false),
                    TwelvePm = table.Column<bool>(nullable: false),
                    TwoAm = table.Column<bool>(nullable: false),
                    TwoPm = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Availibility", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Availibility_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Availibility_UserId",
                table: "Availibility",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Availibility");
        }
    }
}
