﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using QuickRoster.API.Entities;

namespace QuickRoster.API.Migrations
{
    [DbContext(typeof(QuickRosterDbContext))]
    [Migration("20170316025844_databaseInit")]
    partial class databaseInit
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("QuickRoster.API.Entities.Employer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.ToTable("Employer");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.JoiningCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<int>("EmployerId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("JoiningCode");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Roster", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("EmployerId");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.ToTable("Roster");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Shift", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("FinishTime");

                    b.Property<int?>("RosterId");

                    b.Property<int?>("SkillRequiredId");

                    b.Property<DateTime>("StartTime");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("RosterId");

                    b.HasIndex("SkillRequiredId");

                    b.HasIndex("UserId");

                    b.ToTable("Shift");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Skill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("SkillName")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Skill");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<int?>("EmployerId");

                    b.Property<string>("Name");

                    b.Property<int>("Role");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.JoiningCode", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany()
                        .HasForeignKey("EmployerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("QuickRoster.API.Entities.User", "User")
                        .WithOne("JoiningCode")
                        .HasForeignKey("QuickRoster.API.Entities.JoiningCode", "UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Roster", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany("Rosters")
                        .HasForeignKey("EmployerId");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Shift", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Roster", "Roster")
                        .WithMany("Shifts")
                        .HasForeignKey("RosterId");

                    b.HasOne("QuickRoster.API.Entities.Skill", "SkillRequired")
                        .WithMany()
                        .HasForeignKey("SkillRequiredId");

                    b.HasOne("QuickRoster.API.Entities.User", "User")
                        .WithMany("Shifts")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Skill", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.User")
                        .WithMany("Skills")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.User", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany("Employees")
                        .HasForeignKey("EmployerId");
                });
        }
    }
}
