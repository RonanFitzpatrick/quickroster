﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class fixingErrors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_ShiftTemplates_TemplateId",
                table: "Shift");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_Shift_TemplateId",
                table: "Shift");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Shift",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "TemplateId",
                table: "Shift",
                newName: "TemplateID");

            migrationBuilder.RenameIndex(
                name: "IX_Shift_UserId",
                table: "Shift",
                newName: "IX_Shift_UserID");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "Shift",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TemplateID",
                table: "Shift",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShiftTemplateId",
                table: "Shift",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Shift_ShiftTemplateId",
                table: "Shift",
                column: "ShiftTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_ShiftTemplates_ShiftTemplateId",
                table: "Shift",
                column: "ShiftTemplateId",
                principalTable: "ShiftTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_User_UserID",
                table: "Shift",
                column: "UserID",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_ShiftTemplates_ShiftTemplateId",
                table: "Shift");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_User_UserID",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_Shift_ShiftTemplateId",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "ShiftTemplateId",
                table: "Shift");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "Shift",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "TemplateID",
                table: "Shift",
                newName: "TemplateId");

            migrationBuilder.RenameIndex(
                name: "IX_Shift_UserID",
                table: "Shift",
                newName: "IX_Shift_UserId");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Shift",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TemplateId",
                table: "Shift",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Shift_TemplateId",
                table: "Shift",
                column: "TemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_ShiftTemplates_TemplateId",
                table: "Shift",
                column: "TemplateId",
                principalTable: "ShiftTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
