﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace QuickRoster.API.Migrations
{
    public partial class rosterTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_Skill_SkillRequiredId",
                table: "Shift");

            migrationBuilder.DropForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift");

            migrationBuilder.DropIndex(
                name: "IX_Shift_SkillRequiredId",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "FinishTime",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "SkillRequiredId",
                table: "Shift");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Shift");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Shift",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "Templates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Templates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Templates_Employer_EmployerId",
                        column: x => x.EmployerId,
                        principalTable: "Employer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShiftTemplates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Day = table.Column<int>(nullable: false),
                    FinishHour = table.Column<short>(nullable: false),
                    SkillRequiredId = table.Column<int>(nullable: false),
                    StartHour = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShiftTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShiftTemplates_Skill_SkillRequiredId",
                        column: x => x.SkillRequiredId,
                        principalTable: "Skill",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Templates_EmployerId",
                table: "Templates",
                column: "EmployerId");

            migrationBuilder.CreateIndex(
                name: "IX_ShiftTemplates_SkillRequiredId",
                table: "ShiftTemplates",
                column: "SkillRequiredId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift");

            migrationBuilder.DropTable(
                name: "Templates");

            migrationBuilder.DropTable(
                name: "ShiftTemplates");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Shift",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FinishTime",
                table: "Shift",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "SkillRequiredId",
                table: "Shift",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Shift",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_Shift_SkillRequiredId",
                table: "Shift",
                column: "SkillRequiredId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_Skill_SkillRequiredId",
                table: "Shift",
                column: "SkillRequiredId",
                principalTable: "Skill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_User_UserId",
                table: "Shift",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
