﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class addingshiftstorostertemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RosterTemplateId",
                table: "ShiftTemplates",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ShiftTemplates_RosterTemplateId",
                table: "ShiftTemplates",
                column: "RosterTemplateId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShiftTemplates_RosterTemplates_RosterTemplateId",
                table: "ShiftTemplates",
                column: "RosterTemplateId",
                principalTable: "RosterTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShiftTemplates_RosterTemplates_RosterTemplateId",
                table: "ShiftTemplates");

            migrationBuilder.DropIndex(
                name: "IX_ShiftTemplates_RosterTemplateId",
                table: "ShiftTemplates");

            migrationBuilder.DropColumn(
                name: "RosterTemplateId",
                table: "ShiftTemplates");
        }
    }
}
