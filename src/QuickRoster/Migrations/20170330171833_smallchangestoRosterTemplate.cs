﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class smallchangestoRosterTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Templates_Employer_EmployerId",
                table: "Templates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Templates",
                table: "Templates");

            migrationBuilder.RenameTable(
                name: "Templates",
                newName: "RosterTemplates");

            migrationBuilder.RenameIndex(
                name: "IX_Templates_EmployerId",
                table: "RosterTemplates",
                newName: "IX_RosterTemplates_EmployerId");

            migrationBuilder.AlterColumn<int>(
                name: "EmployerId",
                table: "RosterTemplates",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TemplateName",
                table: "RosterTemplates",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RosterTemplates",
                table: "RosterTemplates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RosterTemplates_Employer_EmployerId",
                table: "RosterTemplates",
                column: "EmployerId",
                principalTable: "Employer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RosterTemplates_Employer_EmployerId",
                table: "RosterTemplates");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RosterTemplates",
                table: "RosterTemplates");

            migrationBuilder.DropColumn(
                name: "TemplateName",
                table: "RosterTemplates");

            migrationBuilder.RenameTable(
                name: "RosterTemplates",
                newName: "Templates");

            migrationBuilder.RenameIndex(
                name: "IX_RosterTemplates_EmployerId",
                table: "Templates",
                newName: "IX_Templates_EmployerId");

            migrationBuilder.AlterColumn<int>(
                name: "EmployerId",
                table: "Templates",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Templates",
                table: "Templates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Templates_Employer_EmployerId",
                table: "Templates",
                column: "EmployerId",
                principalTable: "Employer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
