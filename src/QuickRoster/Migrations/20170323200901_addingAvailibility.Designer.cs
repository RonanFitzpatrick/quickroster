﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using QuickRoster.API.Entities;

namespace QuickRoster.API.Migrations
{
    [DbContext(typeof(QuickRosterDbContext))]
    [Migration("20170323200901_addingAvailibility")]
    partial class addingAvailibility
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("QuickRoster.API.Entities.Availibility", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Day");

                    b.Property<bool>("EightPm");

                    b.Property<bool>("ElevenAm");

                    b.Property<bool>("ElevenPm");

                    b.Property<bool>("FiveAm");

                    b.Property<bool>("FivePm");

                    b.Property<bool>("FourAm");

                    b.Property<bool>("FourPm");

                    b.Property<bool>("NineAm");

                    b.Property<bool>("NinePm");

                    b.Property<bool>("OneAm");

                    b.Property<bool>("OnePm");

                    b.Property<bool>("SevenAm");

                    b.Property<bool>("SevenPm");

                    b.Property<bool>("SixAm");

                    b.Property<bool>("SixPm");

                    b.Property<bool>("TenAm");

                    b.Property<bool>("TenPm");

                    b.Property<bool>("ThreeAm");

                    b.Property<bool>("Threem");

                    b.Property<bool>("TwelveAm");

                    b.Property<bool>("TwelvePm");

                    b.Property<bool>("TwoAm");

                    b.Property<bool>("TwoPm");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Availibility");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Employer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.ToTable("Employer");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.JoiningCode", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code");

                    b.Property<int>("EmployerId");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("JoiningCode");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Roster", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("EmployerId");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.ToTable("Roster");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Shift", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("FinishTime");

                    b.Property<int?>("RosterId");

                    b.Property<int?>("SkillRequiredId");

                    b.Property<DateTime>("StartTime");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("RosterId");

                    b.HasIndex("SkillRequiredId");

                    b.HasIndex("UserId");

                    b.ToTable("Shift");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Skill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("SkillName")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Skill");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<int?>("EmployerId");

                    b.Property<string>("Name");

                    b.Property<int?>("Role");

                    b.HasKey("Id");

                    b.HasIndex("EmployerId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Availibility", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.User")
                        .WithMany("Availibilities")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.JoiningCode", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany()
                        .HasForeignKey("EmployerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("QuickRoster.API.Entities.User", "User")
                        .WithOne("JoiningCode")
                        .HasForeignKey("QuickRoster.API.Entities.JoiningCode", "UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Roster", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany("Rosters")
                        .HasForeignKey("EmployerId");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Shift", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Roster", "Roster")
                        .WithMany("Shifts")
                        .HasForeignKey("RosterId");

                    b.HasOne("QuickRoster.API.Entities.Skill", "SkillRequired")
                        .WithMany()
                        .HasForeignKey("SkillRequiredId");

                    b.HasOne("QuickRoster.API.Entities.User", "User")
                        .WithMany("Shifts")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("QuickRoster.API.Entities.Skill", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.User")
                        .WithMany("Skills")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("QuickRoster.API.Entities.User", b =>
                {
                    b.HasOne("QuickRoster.API.Entities.Employer", "Employer")
                        .WithMany("Employees")
                        .HasForeignKey("EmployerId");
                });
        }
    }
}
