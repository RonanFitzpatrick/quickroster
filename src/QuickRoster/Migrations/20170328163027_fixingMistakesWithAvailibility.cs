﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class fixingMistakesWithAvailibility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Availibility_User_UserId",
                table: "Availibility");

            migrationBuilder.RenameColumn(
                name: "Threem",
                table: "Availibility",
                newName: "ThreePm");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Availibility",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EightAm",
                table: "Availibility",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Availibility_User_UserId",
                table: "Availibility",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Availibility_User_UserId",
                table: "Availibility");

            migrationBuilder.DropColumn(
                name: "EightAm",
                table: "Availibility");

            migrationBuilder.RenameColumn(
                name: "ThreePm",
                table: "Availibility",
                newName: "Threem");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Availibility",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Availibility_User_UserId",
                table: "Availibility",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
