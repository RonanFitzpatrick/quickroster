﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuickRoster.API.Migrations
{
    public partial class fixingErrors2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_Roster_RosterId",
                table: "Shift");

            migrationBuilder.AlterColumn<int>(
                name: "RosterId",
                table: "Shift",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_Roster_RosterId",
                table: "Shift",
                column: "RosterId",
                principalTable: "Roster",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shift_Roster_RosterId",
                table: "Shift");

            migrationBuilder.AlterColumn<int>(
                name: "RosterId",
                table: "Shift",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Shift_Roster_RosterId",
                table: "Shift",
                column: "RosterId",
                principalTable: "Roster",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
