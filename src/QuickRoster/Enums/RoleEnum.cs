﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Enums
{
    public enum RoleEnum
    {
        Admin,
        User
    }
}
