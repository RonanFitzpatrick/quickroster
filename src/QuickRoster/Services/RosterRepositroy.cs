﻿namespace QuickRoster.API.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Entities;
    using IServices;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;

    public class RosterRepository : BaseEntityRepository<Roster>, IRosterRepository
    {
        public RosterRepository(QuickRosterDbContext context) : base(context)
        {

        }

        public async Task<Roster> GetByDate(DateTime date)
        {
            return await this.Context.Roster.Where(x => x.StartDate == date).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Roster>> GetByBusinessIdWithShiftsAndTemplate(int id)
        {
            return await this.Context.Roster.Include(x => x.Shifts).Where(x => x.EmployerId == id).ToListAsync();
        }
    }
}