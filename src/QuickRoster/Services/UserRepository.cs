﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Services
{
    using Entities;
    using IServices;
    using Microsoft.EntityFrameworkCore;

    public class UserRepository: BaseEntityRepository<User>, IUserRepository
    {
        public UserRepository(QuickRosterDbContext context) : base(context)
        {
        }

        public async Task<User> GetByEmail(string email)
        {
            return await this.Context.User.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<User> GetByEmailWithJoiningCode(string email)
        {
            return await this.Context.User.Include(x => x.JoiningCode).FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<User> GetByEmailWithEmployer(string email)
        {
            return await this.Context.User.Include(x => x.Employer).FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<User> GetByEmailWithAvailibility(string email)
        {
            return await this.Context.User.Include(x => x.Availibilities).FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<ICollection<User>> GetByBusinessId(int businessId)
        {
            //todo refine what is gotten
            return await this.Context.User.Where(x => x.EmployerId == businessId).ToListAsync();
        }
    }
}
