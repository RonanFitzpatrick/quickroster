﻿namespace QuickRoster.API.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities;
    using Microsoft.EntityFrameworkCore;

    public class BaseEntityRepository<T> : IRepository<T> where T : class, IEntity
    {
        public QuickRosterDbContext Context { get; }

        public BaseEntityRepository(QuickRosterDbContext context)
        {
            this.Context = context;
        }

        public async Task<List<T>> GetAsync()
        {
            return await this.Context.Set<T>().ToListAsync();
        }

        public Task<T> GetAsync(int id)
        {
            return this.Context.Set<T>().FirstOrDefaultAsync(a => a.Id.Equals(id));
        }

        public async Task DeleteAsync(int id)
        {
            var entity = await this.GetAsync(id);

            if (entity == null)
            {
                return;
            }
            this.Context.Set<T>().Attach(entity);
            this.Context.Set<T>().Remove(entity);
            await this.Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            if (entity == null)
            {
                return;
            }

            this.Context.Set<T>().Remove(entity);
            await this.Context.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(IEnumerable<T> entityList)
        {
            this.Context.Set<T>().AttachRange(entityList);
            this.Context.Set<T>().RemoveRange(entityList);
            await this.Context.SaveChangesAsync();
        }

        public async Task AddAsync(T entity)
        {
            if (entity == null)
            {
                return;
            }

            this.Context.Set<T>().Add(entity);
            await this.Context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            this.Context.Set<T>().Update(entity);
            await this.Context.SaveChangesAsync();
        }
    }
}
