﻿namespace QuickRoster.API.Services
{
    using Entities;
    using IServices;

    public class ShiftTemplateRepository : BaseEntityRepository<ShiftTemplate>, IShiftTemplateRepository
    {
        public ShiftTemplateRepository(QuickRosterDbContext context) : base(context)
        {
        }
    }
}
