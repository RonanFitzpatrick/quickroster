﻿namespace QuickRoster.API.Services
{
    using Entities;
    using IServices;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;

    public class EmployerRepository : BaseEntityRepository<Employer>, IEmployerRepository
    {
        public EmployerRepository(QuickRosterDbContext context) : base(context)
        {
        }

        public async Task<Employer> GetByEmail(string email)
        {
            return await this.Context.Employer.FirstOrDefaultAsync(x => x.Email == email);
        }
    }
}
