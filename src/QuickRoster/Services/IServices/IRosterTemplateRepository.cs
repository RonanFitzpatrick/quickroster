﻿namespace QuickRoster.API.Services.IServices
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities;
    public interface IRosterTemplateRepository : IRepository<RosterTemplate>
    {
        Task<RosterTemplate> GetByName(string name);

        Task<ICollection<string>> GetRosterNamesForEmployer(int employerIdValue);

        Task<RosterTemplate> GetRosterByName(string rosterName);
    }
}
