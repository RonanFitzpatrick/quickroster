﻿namespace QuickRoster.API.Services.IServices
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities;
    public interface IRosterRepository : IRepository<Roster>
    {
        Task<Roster> GetByDate(DateTime date);

        Task<IEnumerable<Roster>> GetByBusinessIdWithShiftsAndTemplate(int id);
    }
}
