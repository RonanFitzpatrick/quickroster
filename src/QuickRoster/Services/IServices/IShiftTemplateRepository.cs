﻿namespace QuickRoster.API.Services.IServices
{
    using Entities;
    public interface IShiftTemplateRepository : IRepository<ShiftTemplate>
    {
    }
}