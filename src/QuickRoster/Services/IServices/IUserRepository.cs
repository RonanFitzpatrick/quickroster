﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Services.IServices
{
    using Entities;

    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByEmail(string email);

        Task<User> GetByEmailWithJoiningCode(string email);

        Task<User> GetByEmailWithEmployer(string email);

        Task<User> GetByEmailWithAvailibility(string email);

        Task<ICollection<User>> GetByBusinessId(int businessId);
    }
}
