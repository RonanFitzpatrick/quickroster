﻿namespace QuickRoster.API.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities;
    public interface IRepository<T> where T : class, IEntity
    {

        QuickRosterDbContext Context { get; }

        Task<List<T>> GetAsync();

        Task<T> GetAsync(int id);

        Task DeleteAsync(int id);

        Task DeleteAsync(T entity);

        Task DeleteRangeAsync(IEnumerable<T> entityList);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);
    }
}
