﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Services.IServices
{
    using Entities;

    public interface IEmployerRepository : IRepository<Employer>
    {
        Task<Employer> GetByEmail(string email);
    }
}
