﻿namespace QuickRoster.API.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Entities;
    using IServices;

    public class AvailibilityRepository : BaseEntityRepository<Availibility>, IAvailibilityRepository
    {
        public AvailibilityRepository(QuickRosterDbContext context) : base(context)
        {
        }

        public async Task DeleteRangeByUserAsync(int userId)
        {
            var range = this.Context.Availibility.Where(x => x.UserId == userId);
            this.Context.Availibility.RemoveRange(range);
            await this.Context.SaveChangesAsync();
        }
    }
}
