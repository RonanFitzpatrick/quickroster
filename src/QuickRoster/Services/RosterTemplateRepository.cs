﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Services
{
    using Entities;
    using IServices;
    using Microsoft.EntityFrameworkCore;

    public class RosterTemplateRepository : BaseEntityRepository<RosterTemplate>, IRosterTemplateRepository
    {
        public RosterTemplateRepository(QuickRosterDbContext context) : base(context)
        {
        }

        public async Task<RosterTemplate> GetByName(string name)
        {
            return await this.Context.RosterTemplates.FirstOrDefaultAsync(x => x.TemplateName.Equals(name));
        }

        public async Task<ICollection<string>> GetRosterNamesForEmployer(int employerId)
        {
            return await this.Context.RosterTemplates.
                Where(x => x.EmployerId.Equals(employerId)).
                Select(x => x.TemplateName).
                ToListAsync();
        }

        public async Task<RosterTemplate> GetRosterByName(string templateName)
        {
            return await this.Context.RosterTemplates
                .Include(x => x.TemplateShifts)
                .FirstOrDefaultAsync(x => x.TemplateName.Equals(templateName));
        }
    }
}
