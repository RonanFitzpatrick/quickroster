﻿namespace QuickRoster.API.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    public class User : BaseEntity
    {
        public Employer Employer { get; set; }

        public int? Role { get; set; }

        public int? EmployerId { get; set; }

        public JoiningCode JoiningCode { get; set; }

        public ICollection<Skill> Skills { get; set; }

        [Required, MaxLength(256), RegularExpression("^[a - z0 - 9](\\.?[a - z0 - 9]){5,}@g(oogle)? mail\\.com$")]
        public string Email { get; set; }

        public ICollection<Shift> Shifts { get; set;}

        public string Name { get; set; }

        public User(string email, string name)
        {
            this.Email = email;
            this.Name = name;
        }

        public ICollection<Availibility> Availibilities { get; set; } = new List<Availibility>
        {
            new Availibility
            {
                    Day = "Monday"
            },
            new Availibility
            {
                    Day = "Tuesday"
            },
                        new Availibility
            {
                    Day = "Wednesday"
            },
            new Availibility
            {
                    Day = "Thursday"
            },
                        new Availibility
            {
                    Day = "Friday"
            },
            new Availibility
            {
                    Day = "Saturday"
            },
                        new Availibility
            {
                    Day = "Sunday"
            }
        };

        public User()
        {

        }

        public static bool ValidEmail(string email)
        {
            return Regex.IsMatch(email, "[a-zA-Z0-9]{0,}([.]?[a-zA-Z0-9]{1,})[@](gmail|google).com");
        }
    }
}
