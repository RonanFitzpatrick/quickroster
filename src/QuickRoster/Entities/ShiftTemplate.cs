﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuickRoster.API.Entities
{
    using System.ComponentModel.DataAnnotations;
    using Enums;

    public class ShiftTemplate :IEntity
    {
        [Required]
        public string StartHour { get; set; }

        [Required]
        public string FinishHour{ get; set; }

        [Required]
        public Day Day { get; set; }

        [Required]
        public Skill SkillRequired { get; set; }

        public int Id { get; set; }

        public ICollection<Shift> Shifts { get; set; }
    }
}
