﻿namespace QuickRoster.API.Entities
{
    using System;
    using System.Collections.Generic;

    public class Roster : BaseEntity
    {
        public Employer Employer { get; set; }

        public int EmployerId { get; set; }

        public DateTime StartDate { get; set; }

        public ICollection<Shift> Shifts { get; set; }
    }
}
