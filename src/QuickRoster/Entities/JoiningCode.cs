﻿namespace QuickRoster.API.Entities
{
    using System;
    using System.Linq;

    public class JoiningCode : IEntity
    {
        public int Id { get; set; }

        public Employer Employer { get; set; }

        public int EmployerId { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

        public string Code { get; set; }


        public void GenerateCode()
        {
            Random random = new Random(25);
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            this.Code = new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
