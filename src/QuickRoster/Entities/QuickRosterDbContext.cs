﻿namespace QuickRoster.API.Entities
{
    using Microsoft.EntityFrameworkCore;

    public class QuickRosterDbContext : DbContext
    {
        public DbSet<Employer> Employer { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<Roster> Roster { get; set; }

        public DbSet<Shift> Shift { get; set; }

        public DbSet<Skill> Skill { get; set; }

        public DbSet<JoiningCode> JoiningCode { get; set; }

        public DbSet<Availibility> Availibility { get; set; }

        public DbSet<RosterTemplate> RosterTemplates { get; set; }

        public DbSet<ShiftTemplate> ShiftTemplates { get; set; }

        public QuickRosterDbContext(DbContextOptions<QuickRosterDbContext> options) : base(options)
        {
            Database.Migrate();
        }

        public QuickRosterDbContext()
        {
        }
    }
}
