﻿namespace QuickRoster.API.Entities
{
    using Microsoft.EntityFrameworkCore;

    public interface IDbContext
    {
        DbSet<T> Set<T>() where T : class;
    }
}
