﻿namespace QuickRoster.API.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Employer : BaseEntity
    {
        public ICollection<User> Employees { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        [MaxLength(256)]
        public string Email { get; set; }

        public ICollection<Roster> Rosters { get; set; }

        public ICollection<RosterTemplate> RosterTemplates { get; set; }
    }
}
