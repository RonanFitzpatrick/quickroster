﻿namespace QuickRoster.API.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Shift : BaseEntity
    {
        public int RosterId { get; set; }

        public int TemplateID { get; set; }

        public int ShiftTemplateID { get; set; }

        public int UserID { get; set; }
    }
}
