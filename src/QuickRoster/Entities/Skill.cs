﻿namespace QuickRoster.API.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Skill : BaseEntity
    {
        [Required]
        [MaxLength(256)]
        public string SkillName { get; set; }
    }
}
