﻿namespace QuickRoster.API.Entities
{
    public class Availibility : IEntity
    {

        public Availibility()
        {

        }
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Day { get; set; }

        public bool TwelveAm { get; set; } = true;

        public bool OneAm { get; set; } = true;

        public bool TwoAm { get; set; } = true;

        public bool ThreeAm { get; set; } = true;

        public bool FourAm { get; set; } = true;

        public bool FiveAm { get; set; } = true;

        public bool SixAm { get; set; } = true;

        public bool SevenAm { get; set; } = true;

        public bool EightAm { get; set; } = true;

        public bool NineAm { get; set; } = true;

        public bool TenAm { get; set; } = true;

        public bool ElevenAm { get; set; } = true;

        public bool TwelvePm { get; set; } = true;

        public bool OnePm { get; set; } = true;

        public bool TwoPm { get; set; } = true;

        public bool ThreePm { get; set; } = true;

        public bool FourPm { get; set; } = true;

        public bool FivePm { get; set; } = true;

        public bool SixPm { get; set; } = true;

        public bool SevenPm { get; set; } = true;

        public bool EightPm { get; set; } = true;

        public bool NinePm { get; set; } = true;

        public bool TenPm { get; set; } = true;

        public bool ElevenPm { get; set; } = true;
    }
}
