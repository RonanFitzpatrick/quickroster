﻿namespace QuickRoster.API
{
    using System;
    using System.IO;
    using Entities;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.PlatformAbstractions;
    using Services;
    using Services.IServices;
    using Swashbuckle.AspNetCore.Swagger;


    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc();

            services.AddDbContext<QuickRosterDbContext>(o => o.UseSqlServer(Configuration.GetConnectionString("AzureDB")));

            services.AddTransient(typeof(IUserRepository), typeof(UserRepository));
            services.AddTransient(typeof(IEmployerRepository), typeof(EmployerRepository));
            services.AddTransient(typeof(IAvailibilityRepository), typeof(AvailibilityRepository));
            services.AddTransient(typeof(IRosterTemplateRepository), typeof(RosterTemplateRepository));
            services.AddTransient(typeof(IRosterRepository), typeof(RosterRepository));
            services.AddTransient(typeof(IShiftTemplateRepository), typeof(ShiftTemplateRepository));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Quick Roster API",
                    Description = "The backend API for the Quick roster App",
                    TermsOfService = "Private",
                    Contact = new Contact { Name = "Ronan Fitzpatrick", Email = "Ronan_fitzpatrick@live.ie"},
                });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "QuickRoster.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseStaticFiles();
            app.UseSwagger();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseMvc();



        }
    }
}
