# Quick Roster #

This application it to help reduce the errors made in staff rostering for shift work. The idea behind it is to create a system that helps employers and employees create and consume the weekly roster easier with fewer mistakes and errors on both sides. A lot of these business are currently using pen and paper that's what I am seeking to replace in this project


## Getting Started ##

To get started you will need to apply for your own google API key and put it in place in the Googleservices.cs file in the QuickRoster.App project. From there it should be runnable locally when the prerequisites are installed.
 
## Prerequisites ##

* Visual studio
* Xamarin
* SQL server
* Android SDKS
* Google API Key

## Tests ##

The tests for this project are simple unit tests that can be run through the Visual studio test explorer by building the project and pressing run all tests.

The tests follow the triple A pattern and are there to ensure previous working functions still work when changes are made.


## Built With ##

* .net core web API - The web framework used
* Nuget - Dependency Management
* Xamarin - Used to generate RSS Feeds
* Xunit and Moq - testing frameworks
* Entity Framework - DBMS

## Versioning ##

We use git as our versioning tool.

Authors
Ronan Fitzpatrick  - Sole designer and Dev.